//
//  DatePickerViewController.m
//  customPickerView
//
//  Created by shimingwei on 14-8-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "DatePickerViewController.h"
#import "DataModel.h"

@interface DatePickerViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectButtonItem;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)select:(UIBarButtonItem *)sender;

@end

@implementation DatePickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[DataModel defaultModel].language isEqualToNumber:@(0)]) {
        self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh-Hans"];
    }else if ([[DataModel defaultModel].language isEqualToNumber:@(1)]){
        self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    }
    
    [self.cancelButtonItem setTitle:[[DataModel defaultModel] localizedStringForKey:@"CANCEL"]];
    [self.selectButtonItem setTitle:[[DataModel defaultModel] localizedStringForKey:@"SELECT"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(datePickerViewControllerClickCancel)]) {
        [_delegate datePickerViewControllerClickCancel];
    }
}

- (IBAction)select:(UIBarButtonItem *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(datePickerViewControllerClickSelect:)]) {
        [_delegate datePickerViewControllerClickSelect:self];
    }
}
@end
