//
//  PickerViewController.m
//  customPickerView
//
//  Created by shimingwei on 14-8-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "LocationPickerViewController.h"
#import "DataModel.h"

@implementation Location


@end

@interface LocationPickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>{
    NSArray *_provinces;
    NSArray *_cities;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectButtonItem;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)select:(UIBarButtonItem *)sender;

@end

@implementation LocationPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.cancelButtonItem setTitle:[[DataModel defaultModel] localizedStringForKey:@"CANCEL"]];
    [self.selectButtonItem setTitle:[[DataModel defaultModel] localizedStringForKey:@"SELECT"]];
    
    _provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProvincesAndCities" ofType:@"plist"]];
    _cities = [[_provinces objectAtIndex:0] objectForKey:@"Cities"];
    
    _selectedLocation = [[Location alloc] init];
    _selectedLocation.state = [[_provinces objectAtIndex:0] objectForKey:@"State"];
    _selectedLocation.city = [[_cities objectAtIndex:0] objectForKey:@"city"];
}

#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [_provinces count];
            break;
        case 1:
            return [_cities count];
            break;
        default:
            return 0;
            break;
    }
}

#pragma mark -
#pragma mark UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 117;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 44;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [[DataModel defaultModel] localizedLocationForKey:[[_provinces objectAtIndex:row] objectForKey:@"State"]];
            break;
        case 1:
            return [[DataModel defaultModel] localizedLocationForKey:[[_cities objectAtIndex:row] objectForKey:@"city"]];
            break;
        default:
            return nil;
            break;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            _cities = [[_provinces objectAtIndex:row] objectForKey:@"Cities"];
            [pickerView selectRow:0 inComponent:1 animated:NO];
            [pickerView reloadComponent:1];
            
            _selectedLocation.state = [[_provinces objectAtIndex:row] objectForKey:@"State"];
            _selectedLocation.city = [[_cities objectAtIndex:0] objectForKey:@"city"];
            break;
        case 1:
            _selectedLocation.city = [[_cities objectAtIndex:row] objectForKey:@"city"];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(locationPickerViewControllerClickCancel)]) {
        [_delegate locationPickerViewControllerClickCancel];
    }
}

- (IBAction)select:(UIBarButtonItem *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(locationPickerViewControllerClickSelect:)]) {
        [_delegate locationPickerViewControllerClickSelect:self];
    }
}

@end
