//
//  DatePickerViewController.h
//  customPickerView
//
//  Created by shimingwei on 14-8-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DatePickerViewController;
@protocol DatePickerViewControllerDelegate <NSObject>

@required
- (void)datePickerViewControllerClickCancel;
- (void)datePickerViewControllerClickSelect:(DatePickerViewController*)datePickerViewController;

@end

@interface DatePickerViewController : UIViewController

@property (nonatomic)id<DatePickerViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
