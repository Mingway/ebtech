//
//  AbbreviationPickerViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "AbbreviationPickerViewController.h"

@interface AbbreviationPickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>{
    NSArray *_abbreviations;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectBarButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

- (IBAction)select:(UIBarButtonItem *)sender;
- (IBAction)cancel:(UIBarButtonItem *)sender;
@end

@implementation AbbreviationPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSInteger row = [_abbreviations indexOfObject:self.selectedAbbreviation];
    if (row != NSNotFound) {
        [self.pickerView selectRow:row inComponent:0 animated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.cancelBarButton setTitle:NSLocalizedString(@"CANCEL", nil)];
    [self.selectBarButton setTitle:NSLocalizedString(@"SELECT", nil)];
    
    NSDictionary *abbreviationDic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Abbreviation" ofType:@"plist"]];
    _abbreviations = abbreviationDic.allValues;
}

- (void)setDefaultSelection:(NSString*)defaultSelection
{
    self.selectedAbbreviation = defaultSelection;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _abbreviations.count;
}

#pragma mark -
#pragma mark UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 117;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 44;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _abbreviations[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedAbbreviation = _abbreviations[row];
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(abbreviationPickerViewControllerClickCancel)]) {
        [_delegate abbreviationPickerViewControllerClickCancel];
    }
}

- (IBAction)select:(UIBarButtonItem *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(abbreviationPickerViewControllerClickSelect:)]) {
        [_delegate abbreviationPickerViewControllerClickSelect:self];
    }
}

@end
