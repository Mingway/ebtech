//
//  CartViewController.h
//  EBTech
//
//  Created by shimingwei on 14-8-13.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CartViewControllerDelegate <NSObject>

- (void)deleteItemViewWithIndex:(NSInteger)index;

@end

@interface CartViewController : UITableViewController

@property (nonatomic)id<CartViewControllerDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *items;

- (void)setSelectedItems:(NSArray *)items;

@end
