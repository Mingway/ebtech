//
//  OrderViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "OrderViewController.h"
#import "SelectedInsuranceItemView.h"
#import "DataModel.h"

@interface OrderViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSString *_location;
    NSString *_plateNumber;
    NSString *_puchaseDate;
    NSString *_marketValue;
    NSArray *_itemViews;
    NSString *_totalPremium;
    DataModel *_dataModel;
}

@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumberTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *insuranceCategoryTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *limitTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *locationCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPremiumTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPremiumLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UIButton *symbolBtn;

- (IBAction)back:(UIButton *)sender;

@end

@implementation OrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.locationCityLabel.text = _location;
    self.plateNumberLabel.text = _plateNumber;
    self.dateLabel.text = _puchaseDate;
    self.priceLabel.text = _marketValue;
    self.totalPremiumLabel.text = _totalPremium;
    [self.tableView reloadData];
}

- (void)setLocation:(NSString *)location plateNumber:(NSString*)plateNumber purchaseDate:(NSString *)purchaseDate marketValue:(NSString*)marketValue itemViews:(NSArray *)itemViews totalPremium:(NSString*)totalPremium{
    _location = location;
    _plateNumber = plateNumber;
    _puchaseDate = purchaseDate;
    _marketValue = marketValue;
    _itemViews = itemViews;
    _totalPremium = totalPremium;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataModel = [DataModel defaultModel];
    
    self.confirmLabel.text = [_dataModel localizedStringForKey:@"CONFIRM_ORDER"];
    self.numberTitleLabel.text = [NSString stringWithFormat:@"%@:ASH8748959048",[_dataModel localizedStringForKey:@"ORDER_NUMBER"]];
    self.cityTitleLabel.text = [_dataModel localizedStringForKey:@"DRIVING_AREA"];
    self.plateNumberTitleLabel.text = [_dataModel localizedStringForKey:@"PLATE_NUMBER"];
    self.dateTitleLabel.text = [_dataModel localizedStringForKey:@"PURCHASE_DATE"];
    self.priceTitleLabel.text = [_dataModel localizedStringForKey:@"MARKET_VALUE"];
    self.insuranceCategoryTitleLabel.text = [_dataModel localizedStringForKey:@"SELECTED_COVERAGE"];
    self.limitTitleLabel.text = [_dataModel localizedStringForKey:@"LIMIT_"];
    self.totalPremiumTitleLabel.text = [_dataModel localizedStringForKey:@"TOTAL_PREMIUM"];
    self.tipsLabel.text = [NSString stringWithFormat:@"*%@",[_dataModel localizedStringForKey:@"PERIOD_OF_INSURANCE"]];
    self.symbolLabel.text = [_dataModel localizedStringForKey:@"SYMBOL"];
    [self.symbolBtn setTitle:[_dataModel localizedStringForKey:@"SYMBOL"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _itemViews.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    SelectedInsuranceItemView *itemView = _itemViews[indexPath.row];
    
    UILabel *categoryLabel = (UILabel *)[cell viewWithTag:1];
    categoryLabel.text = itemView.titleLabel.text;
    
    UILabel *limitLabel = (UILabel*)[cell viewWithTag:2];
    if (itemView.type == Input) {
        limitLabel.text = itemView.limitTextField.text;
    }else{
        limitLabel.text = itemView.limitLabel.text;
    }
    
    return cell;
}

@end
