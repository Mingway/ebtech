//
//  SelectValueTableViewController.h
//  EBTech
//
//  Created by shimingwei on 14-8-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectValueTableViewControllerDelegate <NSObject>

- (void)selectValue:(NSString*)value;

@end

@interface SelectValueTableViewController : UITableViewController

@property (nonatomic)id<SelectValueTableViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray *values;

@end
