//
//  PayViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PayViewController.h"
#import "LocationPickerViewController.h"
#import "DatePickerViewController.h"
#import "SelectedInsuranceItemView.h"
#import "DataModel.h"
#import "OrderViewController.h"
#import "Utils.h"
#import "CommonConstans.h"
#import "SelectValueTableViewController.h"
#import "AbbreviationPickerViewController.h"
#import "GetData.h"
#import "PlanFactor.h"

@interface PayViewController ()<LocationPickerViewControllerDelegate, DatePickerViewControllerDelegate, SelectedInsuranceItemViewDelegate, SelectValueTableViewControllerDelegate, UITextFieldDelegate, AbbreviationPickerViewControllerdelegate>{
    UIPopoverController *_popController;
    NSMutableArray *_itemViews;
    SelectedInsuranceItemView *_currentItemView;
    SelectedInsuranceItemView *_ownDamageItemView;
    DataModel *_dataModel;
    NSMutableArray *_models;
    GetData *_getData;
    BOOL _isCalculated;
    NSDictionary *_calculateDic;
    Location *_selectedLocation;
}
@property (weak, nonatomic) IBOutlet UILabel *cityTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumberTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPremiumTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *orderBtn;
@property (weak, nonatomic) IBOutlet UILabel *estimatedPremiumLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *plateNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *abbreviationBtn;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)back:(UIButton *)sender;
- (IBAction)caculate:(UIButton *)sender;
- (IBAction)selectionBtnClicked:(UIButton *)sender;
- (IBAction)selectAbbreviation:(UIButton *)sender;
- (IBAction)save:(UIButton *)sender;

@end

@implementation PayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    if (_getData) {
        _getData = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataModel = [DataModel defaultModel];
    
    if ([_dataModel getState] && [_dataModel getCity]) {
        _selectedLocation = [[Location alloc] init];
        _selectedLocation.state = [_dataModel getState];
        _selectedLocation.city = [_dataModel getCity];
        self.locationLabel.text = [NSString stringWithFormat:@"%@ / %@",[_dataModel localizedLocationForKey:[_dataModel getState]],[_dataModel localizedLocationForKey:[_dataModel getCity]]];
    }
    self.dateLabel.text = [_dataModel getDateStr];
    [self.abbreviationBtn setTitle:[_dataModel getAbbreviaiton] forState:UIControlStateNormal];
    self.plateNumberTextField.text = [_dataModel getPlateNumber];
    self.priceTextField.text = [_dataModel getPrice];
    
    self.estimatedPremiumLabel.text = [_dataModel localizedStringForKey:@"ESTIMATED_PREMIUM"];
    self.cityTitleLabel.text = [_dataModel localizedStringForKey:@"DRIVING_AREA"];
    self.plateNumberTitleLabel.text = [_dataModel localizedStringForKey:@"PLATE_NUMBER"];
    self.dateTitleLabel.text = [_dataModel localizedStringForKey:@"PURCHASE_DATE"];
    self.priceTitleLabel.text = [_dataModel localizedStringForKey:@"MARKET_VALUE"];
    self.totalPremiumTitleLabel.text = [_dataModel localizedStringForKey:@"TOTAL_PREMIUM"];
    [self.orderBtn setTitle:[_dataModel localizedStringForKey:@"ORDER_DETAIL"] forState:UIControlStateNormal];
    self.symbolLabel.text = [_dataModel localizedStringForKey:@"SYMBOL"];
    
    if (!_getData) {
        _getData = [[GetData alloc] init];
    }
    
    [_getData getPlanFactorsWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        if (isSuccess) {
            _models = [NSMutableArray array];
            NSArray *results = [result objectForKey:@"results"];
            NSDictionary *detail = [results[0] objectForKey:@"detail"];
            NSArray *children= [detail objectForKey:@"children"];
            for (NSDictionary *dic in children) {
                PlanFactor *plan = [[PlanFactor alloc] init];
                [plan updateWithDic:dic];
                [_models addObject:plan];
            }
            
            if (!_itemViews) {
                _itemViews = [NSMutableArray array];
            }
            
            NSArray *selectedInsurance = [[DataModel defaultModel] getInsurance];
            if ([selectedInsurance indexOfObject:@(21)] != NSNotFound) {
                _ownDamageItemView = [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"OWN_DAMAGE"] purchaseIndex:[Utils getPercentWithIndex:21] iconImageName:@"lossOfVehicles" factor:0.02 code:@"OD" defaultValue:[self.priceTextField.text floatValue]];
            }
            
            if ([selectedInsurance indexOfObject:@(22)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"FIXTURE_ACCESSORIES"] purchaseIndex:[Utils getPercentWithIndex:22] iconImageName:@"lossOfEquipment" factor:0.01 code:@"ACCE" defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(23)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"TRANSPORT_SERVICE"] purchaseIndex:[Utils getPercentWithIndex:23] iconImageName:@"travelExpenses" factor:0 code:@"LOU" defaultValue:50.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(24)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"SELF_IGNITION_INSURANCE"] purchaseIndex:[Utils getPercentWithIndex:24] iconImageName:@"icn_spontaneousCombustion" factor:0.1 code:@"FI"defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(25)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"ROBBERY_THEFT"] purchaseIndex:[Utils getPercentWithIndex:25] iconImageName:@"icn_wasRobbery" factor:0.002 code:@"TH" defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(26)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"PEOPLE_LIABILITY"] purchaseIndex:[Utils getPercentWithIndex:26] iconImageName:@"icn_mainDriverInjury" factor:0.001 code:@"PA" defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(27)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"THIRD_LIABILITY"] purchaseIndex:[Utils getPercentWithIndex:27] iconImageName:@"icn_thirdPartyLiabilityLoss" factor:0.001 code:@"TP" defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(28)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"SCRATCH"] purchaseIndex:[Utils getPercentWithIndex:28] iconImageName:@"icn_carBodyScratches" factor:0.08 code:@"SCRAPE" defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(29)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"WINDOW_BREAKAGE"] purchaseIndex:[Utils getPercentWithIndex:29] iconImageName:@"icn_windshieldLoss" factor:0.05 code:@"WIND" defaultValue:0.0f];
            }
            
            if ([selectedInsurance indexOfObject:@(30)] != NSNotFound) {
                [self addItemViewWithFrame:CGRectMake(0, 0, 467, 200) titleStr:[_dataModel localizedStringForKey:@"MORAL_DAMAGE"] purchaseIndex:[Utils getPercentWithIndex:30] iconImageName:@"icn_mentalAnguish" factor:0.01 code:@"MDL" defaultValue:0.0f];
            }
            
            [self resetFrame];
        }
    }];
}

//- (void)caculateTotalPremium
//{
//    float sum = 0.0f;
//    for (SelectedInsuranceItemView *itemView in _itemViews) {
//        sum += [itemView.priceLabel.text floatValue];
//    }
//    self.totalNumberLabel.text = [NSString stringWithFormat:@"%.2f",sum];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)caculate:(UIButton *)sender
{
//    [self caculateTotalPremium];
    [self calculateTotalPremiumWithCompletionHandler:^(BOOL isSuccess) {
        
    }];
}

- (void)calculateTotalPremiumWithCompletionHandler:(void(^)(BOOL isSuccess))completionHandler
{
    NSMutableDictionary *selectionDic = [NSMutableDictionary dictionary];
    NSMutableDictionary *factor_tableDic = [NSMutableDictionary dictionary];
    
    for (PlanFactor *plan in _models) {
        BOOL isExists = NO;
        SelectedInsuranceItemView *existItemView;
        for (SelectedInsuranceItemView *itemView in _itemViews) {
            if ([plan.code isEqualToString:itemView.planFactor.code]) {
                isExists = YES;
                existItemView = itemView;
                break;
            }
        }
        
        [selectionDic setObject:@(isExists) forKey:plan.uuid];
        if (isExists) {
            if (existItemView.type == Input) {
                [factor_tableDic setObject:@([existItemView.limitTextField.text floatValue]) forKey:plan.subCode];
            }else{
                if (plan.subCode) {
                    [factor_tableDic setObject:@([existItemView.limitLabel.text floatValue]) forKey:plan.subCode];
                }else{
                    [factor_tableDic setObject:@([existItemView.limitLabel.text floatValue]) forKey:plan.code];
                }
            }
        }else{
            [factor_tableDic setObject:@(0) forKey:plan.subCode];
        }
    }
    
    [factor_tableDic setObject:@"2014-08-26T02:44:03.880Z" forKey:@"effectiveDate"];
    [factor_tableDic setObject:@"2015-08-25T02:44:03.880Z" forKey:@"expiredDate"];
    
    [_getData calculateWithPlans:factor_tableDic uuids:selectionDic completionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        if (isSuccess) {
            if ([[result objectForKey:@"success"] isEqualToNumber:@(1)]) {
                NSDictionary *calculation = [result objectForKey:@"calculation"];
                _calculateDic = calculation;
                self.totalNumberLabel.text = [NSString stringWithFormat:@"%.2f",[[calculation objectForKey:@"APP"] floatValue]];
                _isCalculated = YES;
                completionHandler(YES);
            }else{
                completionHandler(NO);
            }
        }else{
            completionHandler(NO);
        }
    }];
}

- (void)resetFrame
{
    for (int i = 0; i < _itemViews.count; i++) {
        SelectedInsuranceItemView *itemView = _itemViews[i];
        [itemView setFrame:CGRectMake(30 + (467 + 30) *(1 - (i + 1) % 2 ), 10 + (200 + 10)* (i / 2), 467, 200)];
    }
    
    [self setScrollContentSize];
}

- (void)setScrollContentSize
{
    if (_itemViews && _itemViews.count >= 2) {
        float height = CGRectGetHeight(self.scrollView.frame);
        float height1 = CGRectGetMaxY([(SelectedInsuranceItemView *)_itemViews.lastObject frame]) + 100;
        float height2 = CGRectGetMaxY([(SelectedInsuranceItemView *)_itemViews[_itemViews.count - 2] frame]) + 100;
        if (height1 > height) {
            height = height1;
        }
        if (height2 > height) {
            height = height2;
        }
        self.scrollView.contentSize = CGSizeMake(1024, height);
    }
}

- (SelectedInsuranceItemView *)addItemViewWithFrame:(CGRect)frame titleStr:(NSString *)titleStr purchaseIndex:(NSString *)purchaseIndex iconImageName:(NSString *)iconImageName factor:(float)factor code:(NSString*)code defaultValue:(float)defaultValue
{
    SelectedInsuranceItemView *itemView;
    for (PlanFactor *plan in _models) {
        ItemType type;
        if ([plan.code isEqualToString:code]) {
            if ([plan.type isEqualToString:@"input"]) {
                type = Input;
            }else if ([plan.type isEqualToString:@"option"]){
                type = Choice;
            }else{
                type = Default;
            }
            itemView = [[SelectedInsuranceItemView alloc] initWithFrame:frame titleStr:titleStr purchaseIndex:purchaseIndex iconImageName:iconImageName type:type choiceArr:plan.values defaultValue:defaultValue factor:factor];
            if (type == Choice) {
                itemView.delegate = self;
            }
            itemView.planFactor = plan;
            [self.scrollView addSubview:itemView];
            [_itemViews addObject:itemView];
        }
    }
    return itemView;
}

- (IBAction)selectionBtnClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 1:{
            //行驶城市
            LocationPickerViewController *locationPickerViewController = [[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationPickerViewController"];
            locationPickerViewController.delegate = self;
            _popController = [[UIPopoverController alloc]initWithContentViewController:locationPickerViewController];
            [_popController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
        case 2:{
            //提车时间
            DatePickerViewController *datePickerViewController = [[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
            datePickerViewController.delegate = self;
            _popController = [[UIPopoverController alloc]initWithContentViewController:datePickerViewController];
            [_popController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
        default:
            return;
            break;
    }
}

- (IBAction)selectAbbreviation:(UIButton *)sender
{
    AbbreviationPickerViewController *abbreviationPickerViewController = [[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"AbbreviationPickerViewController"];
    abbreviationPickerViewController.delegate = self;
    NSString *selection = self.abbreviationBtn.titleLabel.text;
    if (![selection isEqualToString:@""]) {
        [abbreviationPickerViewController setDefaultSelection:selection];
    }
    _popController = [[UIPopoverController alloc]initWithContentViewController:abbreviationPickerViewController];
    [_popController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)save:(UIButton *)sender {
    if ([self.locationLabel.text isEqualToString:@""] || [self.plateNumberTextField.text isEqualToString:@""] || [self.dateLabel.text isEqualToString:@""] || [self.priceTextField.text isEqualToString:@""]) {
        [Utils showAlertWithMsg:WARNING_FILL_BLANK];
        return;
    }
    if (!_isCalculated) {
        [self calculateTotalPremiumWithCompletionHandler:^(BOOL isSuccess) {
            if (isSuccess) {
                [self saveAndPushToNextViewController];
            }
        }];
    }else{
        [self saveAndPushToNextViewController];
    }
}

- (void)saveAndPushToNextViewController
{
    [_getData saveOrderWithCalculation:_calculateDic completionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        if (isSuccess) {
            [_dataModel setState:_selectedLocation.state city:_selectedLocation.city dateStr:self.dateLabel.text abbreviation:self.abbreviationBtn.titleLabel.text plateNumber:self.plateNumberTextField.text price:self.priceTextField.text];
            
            OrderViewController *orderViewController = [[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
            [orderViewController setLocation:self.locationLabel.text plateNumber:[NSString stringWithFormat:@"%@%@",self.abbreviationBtn.titleLabel.text,self.plateNumberTextField.text] purchaseDate:self.dateLabel.text marketValue:self.priceTextField.text itemViews:_itemViews totalPremium:self.totalNumberLabel.text];
            [self.navigationController pushViewController:orderViewController animated:YES];
        }
    }];
}

#pragma mark -
#pragma mark PickerViewControllerDelegate

- (void)locationPickerViewControllerClickCancel
{
    [_popController dismissPopoverAnimated:YES];
}

- (void)locationPickerViewControllerClickSelect:(LocationPickerViewController *)locationPickerViewController
{
    [_popController dismissPopoverAnimated:YES];
    _selectedLocation = locationPickerViewController.selectedLocation;
    NSLog(@"%@,%@",locationPickerViewController.selectedLocation.state,locationPickerViewController.selectedLocation.city);
    self.locationLabel.text = [NSString stringWithFormat:@"%@ / %@",[_dataModel localizedLocationForKey:locationPickerViewController.selectedLocation.state],[_dataModel localizedLocationForKey:locationPickerViewController.selectedLocation.city]];
    
    NSDictionary *abbreviationDic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Abbreviation" ofType:@"plist"]];
    NSString *abbreviation = [abbreviationDic objectForKey:locationPickerViewController.selectedLocation.state];
    if (abbreviation) {
        [self.abbreviationBtn setTitle:abbreviation forState:UIControlStateNormal];
    }else{
        [self.abbreviationBtn setTitle:[abbreviationDic objectForKey:locationPickerViewController.selectedLocation.city] forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark DatePickerViewControllerDelegate

- (void)datePickerViewControllerClickCancel
{
    [_popController dismissPopoverAnimated:YES];
}

- (void)datePickerViewControllerClickSelect:(DatePickerViewController *)datePickerViewController
{
    [_popController dismissPopoverAnimated:YES];
    NSLog(@"%@",datePickerViewController.datePicker.date);
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    
    comps = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                        fromDate:datePickerViewController.datePicker.date];
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    self.dateLabel.text = [NSString stringWithFormat:@"%li/%li/%li",(long)year,(long)month,(long)day];
}

#pragma mark -
#pragma mark SelectedInsuranceItemViewDelegate

- (void)selectedValuesForSelectedInsuranceItemView:(SelectedInsuranceItemView *)selectedInsuranceItemView
{
    SelectValueTableViewController *selectValueTableViewController = [[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectValueTableViewController"];
    selectValueTableViewController.delegate = self;
    selectValueTableViewController.values = selectedInsuranceItemView.choiceArr;
    _popController = [[UIPopoverController alloc]initWithContentViewController:selectValueTableViewController];
    CGRect rect = [self.scrollView convertRect:selectedInsuranceItemView.frame toView:self.view];
    [_popController presentPopoverFromRect:CGRectMake(CGRectGetMinX(rect) - 40, CGRectGetMinY(rect) - 56, CGRectGetWidth(rect), CGRectGetHeight(rect)) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    _currentItemView = selectedInsuranceItemView;
}

#pragma mark -
#pragma mark SelectValueTableViewControllerdelegate

- (void)selectValue:(NSString *)value
{
    [_popController dismissPopoverAnimated:YES];
    [_currentItemView setLimit:value];
}

#pragma mark -
#pragma mark AbbreviationPickerViewControllerDelegate

- (void)abbreviationPickerViewControllerClickCancel
{
    [_popController dismissPopoverAnimated:YES];
}

- (void)abbreviationPickerViewControllerClickSelect:(AbbreviationPickerViewController *)abbreviationPickerViewController
{
    [_popController dismissPopoverAnimated:YES];
    [self.abbreviationBtn setTitle:abbreviationPickerViewController.selectedAbbreviation forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (_ownDamageItemView) {
        _ownDamageItemView.limitTextField.text = textField.text;
    }
    return YES;
}

@end
