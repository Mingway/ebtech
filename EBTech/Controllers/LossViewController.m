//
//  LossViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "LossViewController.h"
#import "DataModel.h"
#import "LossItemView.h"
#import "UIImageView+PlayGIF.h"
#import "CartViewController.h"

static float kCartAnimationDuration = 1.0f;
static float kScrollViewAnimationDuration = 1.0f;
static float kScrollViewOffsetY = 217.0f;

//static float kFloatAnimationDuration = 4.0f;
static float kFloatRadius = 20.0f;
//static float kFloatAnimationDelay = -0.0f;

static float kRefreshAnimationDuration = 1.0f;

@interface LossViewController ()<LossItemViewDelegate, CartViewControllerDelegate>{
    NSArray *_basicItemViews;
    NSMutableArray *_optionalItemViews;
    NSMutableDictionary *_selectionDics;
    UIPopoverController *_popController;
    BOOL _isAnimationComplete;
    UIView *_optionalView;
    DataModel *_dataModel;
    NSMutableArray *_selectedItemViews;
}
@property (weak, nonatomic) IBOutlet UILabel *afraidCarLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *cartBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *addNumberLabel;
- (IBAction)displayCartViewController:(UIButton *)sender;

@end

@implementation LossViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    for (LossItemView *itemView in _optionalItemViews) {
        [itemView.iconImageView startGIF];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (LossItemView *itemView in _optionalItemViews) {
        [itemView.iconImageView stopGIF];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataModel = [DataModel defaultModel];
    
    self.afraidCarLabel.text = [_dataModel localizedStringForKey:@"AFRAID"];
    
    _basicItemViews = [NSMutableArray array];
    _optionalItemViews = [NSMutableArray array];
    
    BOOL isAddAll = YES;
    NSArray *carAccidentSelections = [[DataModel defaultModel] getCarAccidents];
    if ([carAccidentSelections count] == 1) {
        if ([carAccidentSelections.firstObject isEqualToNumber:@(5)]) {
            isAddAll = NO;
        }
    }
    
    LossItemView *item1 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:1 title:[_dataModel localizedStringForKey:@"LOSS_OF_VEHICLE"] imageName:@"lossOfVehicles" type:Basic delegate:nil];
    LossItemView *item2 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:2 title:[_dataModel localizedStringForKey:@"REPAIRING_COSTS"] imageName:@"maintenanceCosts" type:Basic delegate:nil];
    LossItemView *item3 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:3 title:[_dataModel localizedStringForKey:@"ROADSIDE_ASSISTANCE"] imageName:@"medicalExpenses" type:Basic delegate:nil];
    LossItemView *item4 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:4 title:[_dataModel localizedStringForKey:@"ACCESSORIES_PROTECTION"] imageName:@"lossOfEquipment" type:Basic delegate:nil];
    LossItemView *item5 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:5 title:[_dataModel localizedStringForKey:@"TRANSPORTATION_EXPENSES"] imageName:@"travelExpenses" type:Basic delegate:nil];
    
    [self.scrollView addSubview:item1];
    [self.scrollView addSubview:item2];
    [self.scrollView addSubview:item4];
    
    if (isAddAll) {
        _basicItemViews = @[item1,item2,item3,item4,item5];
        [self.scrollView addSubview:item3];
        [self.scrollView addSubview:item5];
    }else{
        _basicItemViews = @[item1,item2,item4];
        item3 = nil;
        item5 = nil;
    }
    
    UIImageView *shadowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 130, 1024, 87)];
    shadowImageView.image = [UIImage imageNamed:@"shadow"];
    [self.scrollView addSubview:shadowImageView];
    
    _optionalView = [[UIView alloc] initWithFrame:CGRectMake(0, 217, 1024, 783)];
    [self.scrollView addSubview:_optionalView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setFrame:CGRectMake(30, 583, 67, 64)];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [_optionalView addSubview:backBtn];
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView setContentSize:CGSizeMake(1024, 1000)];
    
    NSNumber *carSelection = [[DataModel defaultModel] getCarSelection];
    
    LossItemView *item11 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:11 title:[_dataModel localizedStringForKey:@"WINDSCREEN_REPAIR"] imageName:@"windshieldLoss" type:Optional delegate:self];
    [self moveAnimationWithView:item11];
    LossItemView *item12 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:12 title:[_dataModel localizedStringForKey:@"DRIVER_ACCIDENT"] imageName:@"mainDriverInjury" type:Optional delegate:self];
    [self moveAnimationWithView:item12];
    LossItemView *item13 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:13 title:[_dataModel localizedStringForKey:@"PASSANGER_ACCIDENT"] imageName:@"passengersInjury" type:Optional delegate:self];
    [self moveAnimationWithView:item13];
    LossItemView *item14 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:14 title:[_dataModel localizedStringForKey:@"THIRD_INJURY"] imageName:@"thirdPartyLiabilityInJury" type:Optional delegate:self];
    [self moveAnimationWithView:item14];
    LossItemView *item15 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:15 title:[_dataModel localizedStringForKey:@"THIRD_DAMAGE"] imageName:@"thirdPartyLiabilityLoss" type:Optional delegate:self];
    [self moveAnimationWithView:item15];
    LossItemView *item16 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:16 title:[_dataModel localizedStringForKey:@"CAR_SCRATCH"] imageName:@"carBodyScratches" type:Optional delegate:self];
    [self moveAnimationWithView:item16];
    LossItemView *item17 = [[LossItemView alloc] initWithFrame:CGRectMake(0, 0, 125, 130) index:17 title:[_dataModel localizedStringForKey:@"MORAL_DAMAGE"] imageName:@"mentalAnguish" type:Optional delegate:self];
    [self moveAnimationWithView:item17];
    
    [_optionalView addSubview:item12];
    [_optionalView addSubview:item13];
    [_optionalView addSubview:item14];
    [_optionalView addSubview:item15];
    [_optionalView addSubview:item17];
    
    if ([carSelection isEqualToNumber:@(3)]) {
        _optionalItemViews = [NSMutableArray arrayWithArray:@[item11,item12,item13,item14,item15,item16,item17]];
        [_optionalView addSubview:item11];
        [_optionalView addSubview:item16];
    }else if ([carSelection isEqualToNumber:@(1)]){
        _optionalItemViews = [NSMutableArray arrayWithArray:@[item12,item13,item14,item15,item16,item17]];
        [_optionalView addSubview:item16];
        item11 = nil;
    }else if ([carSelection isEqualToNumber:@(4)]){
        _optionalItemViews = [NSMutableArray arrayWithArray:@[item11,item12,item13,item14,item15,item17]];
        [_optionalView addSubview:item11];
        item16 = nil;
    }else if ([carSelection isEqualToNumber:@(2)]){
        _optionalItemViews = [NSMutableArray arrayWithArray:@[item12,item13,item14,item15,item17]];
        item11 = nil;
        item16 = nil;
    }
    
    _selectionDics = [NSMutableDictionary dictionary];
    _selectedItemViews = [NSMutableArray arrayWithArray:_basicItemViews];
    
    NSMutableArray *selections = [NSMutableArray arrayWithArray:[_dataModel getLoss]];
    for (int i = 0; i < selections.count - 1; i ++) {
        for (int j=0;j<selections.count -i -1;j++) {
            if ([selections[j] integerValue] > [selections[j + 1] integerValue]) {
                NSNumber *tmp = selections[j];
                selections[j] = selections[j + 1];
                selections[j+1] = tmp;
            }
        }
    }
    
    for (NSNumber *selection in selections) {
        for (LossItemView *itemView in _optionalItemViews) {
            if ([@(itemView.index) isEqualToNumber:selection]) {
                [_selectedItemViews addObject:itemView];
                break;
            }
        }
    }
    
    [self refreshBasicItemViewsFrameWithAnimation:NO];
    [self refreshOptionalItemViewsFrameWithAnimation:NO];
    
    [self performSelector:@selector(startAnimationWithObj:) withObject:_selectedItemViews.firstObject afterDelay:1.0f];
    self.view.userInteractionEnabled = NO;
}

- (void)refreshBasicItemViewsFrameWithAnimation:(BOOL)animation
{
    for (int i = 0; i < _basicItemViews.count ;i ++) {
        LossItemView *itemView= _basicItemViews[i];
        if (animation) {
            [UIView animateWithDuration:kRefreshAnimationDuration animations:^{
                [itemView setFrame:CGRectMake(30 + (125 + 85)*i, 40, 125, 130)];
            }];
        }else{
            [itemView setFrame:CGRectMake(30 + (125 + 85)*i, 40, 125, 130)];
        }
    }
}

- (void)refreshOptionalItemViewsFrameWithAnimation:(BOOL)animation
{
    for (int i = 0; i < _optionalItemViews.count; i ++) {
        LossItemView *itemView = _optionalItemViews[i];
        if (animation) {
            [UIView animateWithDuration:kRefreshAnimationDuration animations:^{
                [itemView setFrame:CGRectMake(30 + (125 + 85)*(i % 5) - kFloatRadius/2, 63 + 200*(i / 5), 125, 130)];
            }];
        }else{
            [itemView setFrame:CGRectMake(30 + (125 + 85)*(i % 5) - kFloatRadius/2, 63 + 200*(i / 5), 125, 130)];
        }
    }
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshNumLabel
{
    self.numLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)_selectionDics.count];
}

- (void)startAnimationWithObj:(LossItemView*)obj
{
    if (obj.type == Basic) {
        [self addCartAnimationWithObj:obj offset:CGPointZero];
    }else{
        [self addCartAnimationWithObj:obj offset:CGPointMake(0, -kScrollViewOffsetY)];
        [_selectionDics setObject:obj forKey:@(obj.index)];
        [_optionalItemViews removeObject:obj];
    }
    [_selectionDics setObject:obj forKey:@(obj.index)];
    NSInteger index = [_selectedItemViews indexOfObject:obj];
    if (index != _selectedItemViews.count - 1) {
        [self performSelector:@selector(startAnimationWithObj:) withObject:_selectedItemViews[index + 1] afterDelay:kCartAnimationDuration];
    }else{
        [self performSelector:@selector(scrollViewAnimation) withObject:nil afterDelay:kCartAnimationDuration];
    }
}

- (void)scrollViewAnimation
{
    [UIView animateWithDuration:kScrollViewAnimationDuration animations:^{
       [self.scrollView setContentOffset:CGPointMake(0, kScrollViewOffsetY) animated:NO];
    } completion:^(BOOL finished) {
        if (finished) {
            self.nextBtn.enabled = YES;
            _isAnimationComplete = YES;
            [self refreshOptionalItemViewsFrameWithAnimation:YES];
            self.view.userInteractionEnabled = YES;
        }
    }];
}

- (void)addCartAnimationWithObj:(LossItemView*)obj offset:(CGPoint)offset
{
    [obj.layer removeAllAnimations];
    if (obj.type == Basic) {
       [self.scrollView bringSubviewToFront:obj];
    }else{
        [_optionalView bringSubviewToFront:obj];
    }
    CGPoint endPoint = CGPointMake(self.cartBtn.center.x + offset.x, self.cartBtn.center.y - CGRectGetMinY(self.scrollView.frame) + offset.y);
    UIBezierPath *path=[UIBezierPath bezierPath];
    CGPoint startPoint = obj.center;
    float sx = startPoint.x;
    float sy = startPoint.y;
    float ex = endPoint.x;
    float ey = endPoint.y;
    float x = sx+(ex-sx)/3;
    float y = sy+(ey-sy)*0.5-400;
    CGPoint centerPoint = CGPointMake(x,y);
    [path moveToPoint:startPoint];
    [path addQuadCurveToPoint:endPoint controlPoint:centerPoint];
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.path = path.CGPath;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.duration = kCartAnimationDuration;
    animation.delegate = self;
    animation.autoreverses = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    [obj.layer addAnimation:animation forKey:@"addCart"];
    [self performSelector:@selector(hideItemView:) withObject:obj afterDelay:kCartAnimationDuration];
    if (_isAnimationComplete && obj.type == Optional) {
        [self performSelector:@selector(refreshOptionalViewWithAnimation) withObject:nil afterDelay:kCartAnimationDuration];
    }
    [self performSelector:@selector(refreshNumLabel) withObject:nil afterDelay:kCartAnimationDuration];
    [UIView animateWithDuration:kCartAnimationDuration animations:^{
        obj.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
    }];
}

- (void)hideItemView:(LossItemView*)itemView
{
    itemView.hidden = YES;
    [itemView.layer removeAllAnimations];
    
    self.addNumberLabel.text = @"+1";
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [CATransaction commit];
    
    CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    positionAnimation.fromValue = [NSValue valueWithCGPoint:CGPointMake(self.addNumberLabel.frame.origin.x+30, self.addNumberLabel.frame.origin.y+20)];
    positionAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.addNumberLabel.frame.origin.x+30, self.addNumberLabel.frame.origin.y)];
    
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    opacityAnimation.toValue = [NSNumber numberWithFloat:0];
    
    CABasicAnimation *rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotateAnimation.fromValue = [NSNumber numberWithFloat:0 * M_PI];
    rotateAnimation.toValue = [NSNumber numberWithFloat:2 * M_PI];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.beginTime = CACurrentMediaTime();
    group.duration = 0.3;
    group.animations = [NSArray arrayWithObjects:positionAnimation,opacityAnimation,nil];
    group.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    group.delegate = self;
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    group.autoreverses= NO;
    [self.addNumberLabel.layer addAnimation:group forKey:@"opacity"];
}

- (void)moveAnimationWithView:(LossItemView*)view{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:kFloatAnimationDuration];
    
//    [UIView animateWithDuration:kFloatAnimationDuration animations:^{
//        CGFloat x = (CGFloat) (arc4random() % (int) kFloatRadius);
//        CGFloat y = (CGFloat) (arc4random() % (int) kFloatRadius);
//        
//        NSInteger index = [_optionalItemViews indexOfObject:view];
//        
//        CGPoint squarePostion = CGPointMake(30 + (125 + 85)*(index % 5) + 125/2 + x,63 + 200*(index / 5) + 130/2 + y);
//        view.center = squarePostion;
//    }];
    
//    [UIView setAnimationDelegate:nil];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//    [UIView commitAnimations];
    
//    [self performSelector:@selector(moveAnimationWithView:) withObject:view afterDelay:kFloatAnimationDuration + kFloatAnimationDelay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)displayCartViewController:(UIButton *)sender {
    CartViewController *cartViewController = [[UIStoryboard storyboardWithName:@"Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];    _popController = [[UIPopoverController alloc] initWithContentViewController:cartViewController];
    cartViewController.delegate = self;
    NSMutableArray *indexs = [NSMutableArray arrayWithArray:_selectionDics.allKeys];
    for (int i = 0; i < indexs.count - 1; i ++) {
        for (int j=0;j<indexs.count -i -1;j++) {
            if ([indexs[j] integerValue] < [indexs[j + 1] integerValue]) {
                NSNumber *tmp = indexs[j];
                indexs[j] = indexs[j + 1];
                indexs[j+1] = tmp;
            }
        }
    }
    [cartViewController setSelectedItems:indexs];
    [_popController presentPopoverFromRect:self.cartBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

#pragma mark -
#pragma mark LossItemViewDelegate

- (void)tapItemView:(LossItemView *)itemView
{
    if (_isAnimationComplete || itemView.type == Basic) {
        [_selectionDics setObject:itemView forKey:@(itemView.index)];
        [_optionalItemViews removeObject:itemView];
        [self addCartAnimationWithObj:itemView offset:CGPointZero];
    }else{
        [_selectionDics setObject:itemView forKey:@(itemView.index)];
        [_optionalItemViews removeObject:itemView];
        [self addCartAnimationWithObj:itemView offset:CGPointMake(0, -kScrollViewOffsetY)];
    }
}

#pragma mark -
#pragma mark CartViewControllerDelegate

- (void)deleteItemViewWithIndex:(NSInteger)index
{
    LossItemView *itemView = [_selectionDics objectForKey:@(index)];
    itemView.hidden = NO;
    [itemView removeInsuranceIconImageView];
    if (index < [(LossItemView *)_optionalItemViews.firstObject index]) {
        [_optionalItemViews insertObject:itemView atIndex:0];
    }else if (index > [(LossItemView *)_optionalItemViews.lastObject index]){
        [_optionalItemViews addObject:itemView];
    }else{
        for (int i = 0; i < _optionalItemViews.count - 1; i ++) {
            LossItemView *itemView1 = _optionalItemViews[i];
            LossItemView *itemView2 = _optionalItemViews[i + 1];
            if (index > itemView1.index && index < itemView2.index) {
                [_optionalItemViews insertObject:itemView atIndex:i+1];
                break;
            }
        }
    }
    
    [self refreshNumLabel];
    [self removeCartAnimationWithObj:itemView];
    [self performSelector:@selector(refreshOptionalItemViewsWithAnimation) withObject:nil afterDelay:kCartAnimationDuration];
    [_selectionDics removeObjectForKey:@(index)];
}

- (void)refreshOptionalItemViewsWithAnimation
{
    [self refreshOptionalItemViewsFrameWithAnimation:YES];
}

- (void)removeCartAnimationWithObj:(LossItemView*)obj
{
    [obj.layer removeAllAnimations];
    if (obj.type == Basic) {
        [self.scrollView bringSubviewToFront:obj];
    }else{
        [_optionalView bringSubviewToFront:obj];
    }
    CGPoint endPoint = CGPointMake(self.cartBtn.center.x, self.cartBtn.center.y - CGRectGetMinY(self.scrollView.frame));
    UIBezierPath *path=[UIBezierPath bezierPath];
    CGPoint startPoint = obj.center;
    float sx = startPoint.x;
    float sy = startPoint.y;
    float ex = endPoint.x;
    float ey = endPoint.y;
    float x = sx+(ex-sx)/3;
    float y = sy+(ey-sy)*0.5-400;
    CGPoint centerPoint = CGPointMake(x,y);
    [path moveToPoint:endPoint];
    [path addQuadCurveToPoint:startPoint controlPoint:centerPoint];
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.path = path.CGPath;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.duration = kCartAnimationDuration;
    animation.delegate = self;
    animation.autoreverses = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    [obj.layer addAnimation:animation forKey:@"removeCart"];
    [self performSelector:@selector(refreshNumLabel) withObject:nil afterDelay:kCartAnimationDuration];
    [obj.layer performSelector:@selector(removeAllAnimations) withObject:nil afterDelay:kCartAnimationDuration];
    
    [UIView animateWithDuration:kCartAnimationDuration animations:^{
        obj.layer.transform = CATransform3DIdentity;;
    }];
}

- (void)refreshOptionalViewWithAnimation
{
    [self refreshOptionalItemViewsFrameWithAnimation:YES];
}

#pragma mark -
#pragma mark AnimationDelegate

- (void)animationDidStart:(CAAnimation *)anim
{
//    NSLog(@"开始加入购物车动画");
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
//    NSLog(@"结束动画");
}

#pragma mark -
#pragma mark Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[DataModel defaultModel] setLoss:_selectionDics.allKeys];
}

@end
