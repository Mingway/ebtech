//
//  PickerViewController.h
//  customPickerView
//
//  Created by shimingwei on 14-8-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Location : NSObject

@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *city;

@end

@class LocationPickerViewController;
@protocol LocationPickerViewControllerDelegate <NSObject>

@required
- (void)locationPickerViewControllerClickCancel;
- (void)locationPickerViewControllerClickSelect:(LocationPickerViewController *)locationPickerViewController;

@end

@interface LocationPickerViewController : UIViewController

@property (nonatomic)id<LocationPickerViewControllerDelegate> delegate;
@property (nonatomic, strong) Location *selectedLocation;

@end
