//
//  CarAccidentViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-11.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "CarAccidentViewController.h"
#import "UIButton+Extensions.h"
#import "UIImageView+PlayGIF.h"

#import "Utils.h"
#import "CommonConstans.h"

#import "DataModel.h"

static CGPoint kSmashedViewCenter = {132, 329};
static CGPoint kSufferedDisasterViewCenter = {314, 564};
static CGPoint kCollisionViewCenter = {403, 173};
static CGPoint kSpontaneousCombustionViewCenter = {510, 450};
static CGPoint kWasRobberyViewCenter = {688, 248};

static float kAnimationDuration = 3.0f;
static float kAnimationDelay = -0.4f;
static float kAnimationRadius = 25.0f;

@interface CarAccidentViewController (){
    NSMutableArray *_selectedIndexs;
    DataModel *_dataModel;
}
@property (weak, nonatomic) IBOutlet UILabel *afraidLabel;
//被砸
@property (weak, nonatomic) IBOutlet UILabel *smashedTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *smashedTextBGImageView;
@property (weak, nonatomic) IBOutlet UIImageView *smashedImageView;
@property (weak, nonatomic) IBOutlet UIView *smashedView;
//遭遇灾害
@property (weak, nonatomic) IBOutlet UIImageView *sufferedDisasterImageView;
@property (weak, nonatomic) IBOutlet UIView *sufferedDisasterView;
@property (weak, nonatomic) IBOutlet UILabel *sufferDisasterTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sufferedDisasterTextBGImageView;
//被碰撞
@property (weak, nonatomic) IBOutlet UIImageView *collisionImageView;
@property (weak, nonatomic) IBOutlet UIView *collisionView;
@property (weak, nonatomic) IBOutlet UIImageView *collisionTextBGImageView;
@property (weak, nonatomic) IBOutlet UILabel *collisionTextLabel;
//自燃
@property (weak, nonatomic) IBOutlet UIImageView *spontaneousCombustionImageView;
@property (weak, nonatomic) IBOutlet UIView *spontaneousCombustionView;
@property (weak, nonatomic) IBOutlet UILabel *spontaneousCombustionTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *spontaneousCombustionTextBGImageView;
//被盗抢
@property (weak, nonatomic) IBOutlet UIImageView *wasRobberyImageView;
@property (weak, nonatomic) IBOutlet UIView *wasRobberyView;
@property (weak, nonatomic) IBOutlet UILabel *wasRobberyTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *wasRobberyTextBGImageView;
@property (weak, nonatomic) IBOutlet UILabel *topTextLabel;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)back:(UIButton *)sender;

@end

@implementation CarAccidentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
 
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.smashedImageView startGIF];
    [self.sufferedDisasterImageView startGIF];
    [self.collisionImageView startGIF];
    [self.spontaneousCombustionImageView startGIF];
    [self.wasRobberyImageView startGIF];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.smashedImageView stopGIF];
    [self.sufferedDisasterImageView stopGIF];
    [self.collisionImageView stopGIF];
    [self.spontaneousCombustionImageView stopGIF];
    [self.wasRobberyImageView stopGIF];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataModel = [DataModel defaultModel];
    
    self.afraidLabel.text = [_dataModel localizedStringForKey:@"AFRAID_CAR"];
    self.smashedTextLabel.text = [_dataModel localizedStringForKey:@"FALLING_OBJECTS"];
    self.sufferDisasterTextLabel.text = [_dataModel localizedStringForKey:@"NATURAL_DISASTER"];
    self.collisionTextLabel.text = [_dataModel localizedStringForKey:@"COLLISION"];
    self.spontaneousCombustionTextLabel.text = [_dataModel localizedStringForKey:@"SELF_IGNITION"];
    self.wasRobberyTextLabel.text = [_dataModel localizedStringForKey:@"THEFT"];
    
    self.backBtn.hitTestEdgeInsets = UIEdgeInsetsMake(-20, -40, -20, -40);
    self.nextBtn.hitTestEdgeInsets = UIEdgeInsetsMake(-20, -40, -20, -40);
    [self startAnimation];
    [self loadGif];
    _selectedIndexs = [NSMutableArray array];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.nextBtn addGestureRecognizer:pan];
    
    /*
     
     处理新旧车选择对选项的影响
     
     */
    NSNumber *carSelection = [[DataModel defaultModel] getCarSelection];
    if ([carSelection isEqualToNumber:@(1)] || [carSelection isEqualToNumber:@(3)]) {
        self.spontaneousCombustionView.hidden = YES;
    }else{
        self.spontaneousCombustionView.hidden = NO;
    }
    
    NSArray *selections = [_dataModel getCarAccidents];
    if (selections) {
        for (NSNumber *selection in selections) {
            if (!self.spontaneousCombustionView.hidden || ![selection isEqualToNumber:@(4)]) {
                [self selectItemWithIndex:[selection integerValue]];
            }
        }
    }
}

- (void)startAnimation
{
    [self animationWithObj:self.smashedView];
    [self animationWithObj:self.sufferedDisasterView];
    [self animationWithObj:self.collisionView];
    [self animationWithObj:self.spontaneousCombustionView];
    [self animationWithObj:self.wasRobberyView];
}

- (void)loadGif
{
    self.smashedImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"smashed" ofType:@"gif"];
    [self.smashedImageView loadImageWithIndex:0];
    UITapGestureRecognizer *smashedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.smashedImageView addGestureRecognizer:smashedTap];
    
    self.sufferedDisasterImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"sufferedDisaster" ofType:@"gif"];
    [self.sufferedDisasterImageView loadImageWithIndex:0];
    UITapGestureRecognizer *sufferedDisasterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.sufferedDisasterImageView addGestureRecognizer:sufferedDisasterTap];
    
    self.collisionImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"collision" ofType:@"gif"];
    [self.collisionImageView loadImageWithIndex:0];
    UITapGestureRecognizer *collisonTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.collisionImageView addGestureRecognizer:collisonTap];
    
    self.spontaneousCombustionImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"spontaneousCombustion" ofType:@"gif"];
    [self.spontaneousCombustionImageView loadImageWithIndex:0];
    UITapGestureRecognizer *spontaneousCombustionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.spontaneousCombustionImageView addGestureRecognizer:spontaneousCombustionTap];
    
    self.wasRobberyImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"wasRobbery" ofType:@"gif"];
    [self.wasRobberyImageView loadImageWithIndex:0];
    UITapGestureRecognizer *wasRobberyTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.wasRobberyImageView addGestureRecognizer:wasRobberyTap];
}

- (void)animationWithObj:(UIView*)obj {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:kAnimationDuration];
    
    CGFloat x = (CGFloat) (arc4random() % (int) kAnimationRadius);
    CGFloat y = (CGFloat) (arc4random() % (int) kAnimationRadius);
    
    CGFloat centerX = 0.0f;
    CGFloat centerY = 0.0f;
    
    switch (obj.tag) {
        case 1:{
            centerX = kSmashedViewCenter.x;
            centerY = kSmashedViewCenter.y;
        }
            break;
        case 2:{
            centerX = kSufferedDisasterViewCenter.x;
            centerY = kSufferedDisasterViewCenter.y;
        }
            break;
        case 3:{
            centerX = kCollisionViewCenter.x;
            centerY = kCollisionViewCenter.y;
        }
            break;
        case 4:{
            centerX = kSpontaneousCombustionViewCenter.x;
            centerY = kSpontaneousCombustionViewCenter.y;
        }
            break;
        case 5:{
            centerX = kWasRobberyViewCenter.x;
            centerY = kWasRobberyViewCenter.y;
        }
            break;
        default:
            break;
    }
    
    CGPoint squarePostion = CGPointMake(centerX + x,centerY + y);
    obj.center = squarePostion;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView commitAnimations];
    
    [self performSelector:@selector(animationWithObj:) withObject:obj afterDelay:kAnimationDuration + kAnimationDelay];
}

- (void)selectItemWithIndex:(NSInteger)index
{
    switch (index) {
        case 1:{
            if ([_selectedIndexs indexOfObject:@(1)] != NSNotFound) {
                self. smashedTextBGImageView.image = [UIImage imageNamed:@"smashedTextBG"];
                [_selectedIndexs removeObject:@(1)];
            }else{
                self. smashedTextBGImageView.image = [UIImage imageNamed:@"smashedTextBG_selected"];
                [_selectedIndexs addObject:@(1)];
            }
        }
            break;
        case 2:{
            if ([_selectedIndexs indexOfObject:@(2)] != NSNotFound) {
                self.sufferedDisasterTextBGImageView.image = [UIImage imageNamed:@"sufferedDisasterTextBG"];
                [_selectedIndexs removeObject:@(2)];
            }else{
                self.sufferedDisasterTextBGImageView.image = [UIImage imageNamed:@"sufferedDisasterTextBG_selected"];
                [_selectedIndexs addObject:@(2)];
            }
        }
            break;
        case 3:{
            if ([_selectedIndexs indexOfObject:@(3)] != NSNotFound) {
                self.collisionTextBGImageView.image = [UIImage imageNamed:@"collisionTextBG"];
                [_selectedIndexs removeObject:@(3)];
            }else{
                self.collisionTextBGImageView.image = [UIImage imageNamed:@"collisionTextBG_selected"];
                [_selectedIndexs addObject:@(3)];
            }
        }
            break;
        case 4:{
            if ([_selectedIndexs indexOfObject:@(4)] != NSNotFound) {
                self.spontaneousCombustionTextBGImageView.image = [UIImage imageNamed:@"spontaneousCombustionTextBG"];
                [_selectedIndexs removeObject:@(4)];
            }else{
                self.spontaneousCombustionTextBGImageView.image = [UIImage imageNamed:@"spontaneousCombustionTextBG_selected"];
                [_selectedIndexs addObject:@(4)];
            }
        }
            break;
        case 5:{
            if ([_selectedIndexs indexOfObject:@(5)] != NSNotFound) {
                self.wasRobberyTextBGImageView.image = [UIImage imageNamed:@"wasRobberyTextBG"];
                [_selectedIndexs removeObject:@(5)];
            }else{
                self.wasRobberyTextBGImageView.image = [UIImage imageNamed:@"wasRobberyTextBG_selected"];
                [_selectedIndexs addObject:@(5)];
            }
        }
            break;
        default:
            return;
            break;
    }
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tap
{
    [self selectItemWithIndex:tap.view.tag];
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)pan
{
    UIView *piece = [pan view];
    
    [self adjustAnchorPointForGestureRecognizer:pan];
    
    if ([pan state] == UIGestureRecognizerStateBegan || [pan state] == UIGestureRecognizerStateChanged) {
        CGPoint translation = [pan translationInView:[piece superview]];
        
        float x = [piece center].x + translation.x;
        float y = [piece center].y + translation.y;
        
        if (x < CGRectGetWidth(piece.frame) / 2) {
            x = CGRectGetWidth(piece.frame) / 2;
        }else if (x > (CGRectGetWidth(self.view.frame) - CGRectGetWidth(piece.frame)/2)){
            x = CGRectGetWidth(self.view.frame) - CGRectGetWidth(piece.frame)/2;
        }
        
        if (y < CGRectGetHeight(piece.frame) / 2) {
            y = CGRectGetHeight(piece.frame) / 2;
        }else if (y > (CGRectGetHeight(self.view.frame) - CGRectGetHeight(piece.frame)/2)){
            y = CGRectGetHeight(self.view.frame) - CGRectGetHeight(piece.frame)/2;
        }
        
        [piece setCenter:CGPointMake( x, y)];
        [pan setTranslation:CGPointZero inView:[piece superview]];
    }
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

#pragma mark -
#pragma mark Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"nextStep"]) {
        if (_selectedIndexs.count == 0) {
            [Utils showAlertWithMsg:WARNING_NO_SELECTION];
            return NO;
        }
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[DataModel defaultModel] setCarAccidents:_selectedIndexs];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
