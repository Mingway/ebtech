//
//  InsuranceChoiceViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-14.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "InsuranceChoiceViewController.h"
#import "InsuranceItemView.h"
#import "Utils.h"
#import "CommonConstans.h"
#import "DataModel.h"

@interface InsuranceChoiceViewController ()<InsuranceItemViewDelegate>{
    NSMutableArray *_itemViews;
    float _offset;
    BOOL _isOpen;
    NSMutableDictionary *_selections;
    DataModel *_dataModel;
}
@property (weak, nonatomic) IBOutlet UILabel *shoppingCartLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)back:(UIButton *)sender;

@end

@implementation InsuranceChoiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataModel = [DataModel defaultModel];
    
    self.shoppingCartLabel.text = [_dataModel localizedStringForKey:@"SHOPPIONG_CART"];
    
    if (!_itemViews) {
        _itemViews = [NSMutableArray array];
    }

    _selections = [NSMutableDictionary dictionary];
    NSArray *selectedCarAccident = [[DataModel defaultModel] getCarAccidents];
    NSArray *selectedLoss = [[DataModel defaultModel] getLoss];
    
    if ([selectedCarAccident indexOfObject:@(1)] != NSNotFound || [selectedCarAccident indexOfObject:@(2)] || [selectedCarAccident indexOfObject:@(3)] || [selectedCarAccident indexOfObject:@(4)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"OWN_DAMAGE_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"OWN_DAMAGE"] purchaseIndex:[Utils getPercentWithIndex:21] iconImageName:@"lossOfVehicles" index:21];
        
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"FIXTURE_ACCESSORIES_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"FIXTURE_ACCESSORIES"] purchaseIndex:[Utils getPercentWithIndex:22] iconImageName:@"lossOfEquipment" index:22];
        
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"TRANSPORT_SERVICE_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"TRANSPORT_SERVICE"] purchaseIndex:[Utils getPercentWithIndex:23] iconImageName:@"travelExpenses" index:23];
    }
    if ([selectedCarAccident indexOfObject:@(4)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"SELF_IGNITION_INSURANCE_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"SELF_IGNITION_INSURANCE"] purchaseIndex:[Utils getPercentWithIndex:24] iconImageName:@"icn_spontaneousCombustion" index:24];
    }
    if ([selectedCarAccident indexOfObject:@(5)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"ROBBERY_THEFT_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"ROBBERY_THEFT"] purchaseIndex:[Utils getPercentWithIndex:25] iconImageName:@"icn_wasRobbery" index:25];
    }
    if ([selectedLoss indexOfObject:@(12)] != NSNotFound || [selectedLoss indexOfObject:@(13)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"PEOPLE_LIABILITY_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"PEOPLE_LIABILITY"] purchaseIndex:[Utils getPercentWithIndex:26] iconImageName:@"icn_mainDriverInjury" index:26];
    }
    if ([selectedLoss indexOfObject:@(14)] != NSNotFound || [selectedLoss indexOfObject:@(15)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"THIRD_LIABILITY_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"THIRD_LIABILITY"] purchaseIndex:[Utils getPercentWithIndex:27] iconImageName:@"icn_thirdPartyLiabilityLoss" index:27];
    }
    if ([selectedLoss indexOfObject:@(16)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"SCRATCH_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"SCRATCH"] purchaseIndex:[Utils getPercentWithIndex:28] iconImageName:@"icn_carBodyScratches" index:28];
    }
    if ([selectedLoss indexOfObject:@(11)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"WINDOW_BREAKAGE_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"WINDOW_BREAKAGE"] purchaseIndex:[Utils getPercentWithIndex:29] iconImageName:@"icn_windshieldLoss" index:29];
    }
    if ([selectedLoss indexOfObject:@(17)] != NSNotFound) {
        [self addItemViewWithFrame:CGRectMake(0, 0, 467, 150) coverageStr:[_dataModel localizedStringForKey:@"MORAL_DAMAGE_CONTENT"] titleStr:[_dataModel localizedStringForKey:@"MORAL_DAMAGE"] purchaseIndex:[Utils getPercentWithIndex:30] iconImageName:@"icn_mentalAnguish" index:30];
    }
    [self resetFrame];
    
    NSArray *selections = [_dataModel getInsurance];
    if (selections) {
        NSMutableArray *selectedItemViews = [NSMutableArray array];
        for (NSNumber *selection in selections) {
            for (InsuranceItemView *itemView in _itemViews) {
                if ([@(itemView.index) isEqualToNumber:selection]) {
                    [selectedItemViews addObject:itemView];
                    break;
                }
            }
        }
        [selectedItemViews makeObjectsPerformSelector:@selector(showOrHide)];
    }
}

- (void)resetFrame
{
    for (int i = 0; i < _itemViews.count; i++) {
        InsuranceItemView *itemView = _itemViews[i];
        [itemView setFrame:CGRectMake(30 + (467 + 30) *(1 - (i + 1) % 2 ), 10 + (150 + 10)* (i / 2), 467, 150)];
    }
    [self setScrollContentSize];
}

- (void)addItemViewWithFrame:(CGRect)frame coverageStr:(NSString*)coverageStr titleStr:(NSString *)titleStr purchaseIndex:(NSString*)purchaseIndex iconImageName:(NSString*)iconImageName index:(NSInteger)index
{
    InsuranceItemView *itemView = [[InsuranceItemView alloc] initWithFrame:frame coverageStr:coverageStr titleStr:titleStr purchaseIndex:purchaseIndex iconImageName:iconImageName index:index delegate:self];
    [self.scrollView addSubview:itemView];
    [_itemViews addObject:itemView];
}

#pragma mark -
#pragma mark InsuranceItemViewDelegate

- (void)insuranceViewClickButton:(InsuranceItemView *)itemView
{
    [UIView animateWithDuration:0.3f animations:^{
        NSInteger index = [_itemViews indexOfObject:itemView];
        for (int i = index ; i < _itemViews.count; i+=2) {
            InsuranceItemView *itemView = _itemViews[i];
            
            if (i == index) {
                _isOpen = itemView.isOpen;
                
                CGSize size = CGSizeMake(420, 10000);
                CGRect rect = [itemView.coverageStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil];
                float height = 100 + rect.size.height + 30;
                if (height < 150) {
                    height = 150;
                }
                
                if (_isOpen) {
                    [UIView animateWithDuration:1.0f animations:^{
                        [itemView setFrame:CGRectMake(CGRectGetMinX(itemView.frame) , CGRectGetMinY(itemView.frame), CGRectGetWidth(itemView.frame), height)];
                        [itemView.bgView setFrame:CGRectMake(CGRectGetMinX(itemView.bgView.frame) , CGRectGetMinY(itemView.bgView.frame) , CGRectGetWidth(itemView.bgView.frame), height - 8)];
                    }];
                    
                    [_selections setObject:itemView forKey:@(itemView.index)];
                }else{
                    [itemView setFrame:CGRectMake(CGRectGetMinX(itemView.frame) , CGRectGetMinY(itemView.frame), CGRectGetWidth(itemView.frame), 150)];
                    [itemView.bgView setFrame:CGRectMake(CGRectGetMinX(itemView.bgView.frame) , CGRectGetMinY(itemView.bgView.frame) , CGRectGetWidth(itemView.bgView.frame), 142)];
                    
                    [_selections removeObjectForKey:@(itemView.index)];
                }
                
                _offset = height - 150;
            }else{
                if (_isOpen) {
                    [itemView setFrame:CGRectOffset(itemView.frame, 0, _offset)];
                }else{
                    [itemView setFrame:CGRectOffset(itemView.frame, 0, -_offset)];
                }
            }
        }
    } completion:^(BOOL finished) {
        [self setScrollContentSize];
    }];
}

- (void)setScrollContentSize
{
    if (_itemViews && _itemViews.count >= 2) {
        float height = CGRectGetHeight(self.scrollView.frame);
        float height1 = CGRectGetMaxY([(InsuranceItemView *)_itemViews.lastObject frame]) + 100;
        float height2 = CGRectGetMaxY([(InsuranceItemView *)_itemViews[_itemViews.count - 2] frame]) + 100;
        if (height1 > height) {
            height = height1;
        }
        if (height2 > height) {
            height = height2;
        }
        self.scrollView.contentSize = CGSizeMake(1024, height);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"pay"]) {
        if (_selections.count == 0) {
            [Utils showAlertWithMsg:WARNING_NO_SELECTION];
            return NO;
        }
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[DataModel defaultModel] setInsurance:_selections.allKeys];
}

@end
