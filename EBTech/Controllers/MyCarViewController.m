//
//  MyCarViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-11.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MyCarViewController.h"
#import "UIImageView+PlayGIF.h"
#import "StarEmitterView.h"
#import "DataModel.h"

#import "Utils.h"
#import "CommonConstans.h"

@import QuartzCore;

@interface MyCarViewController (){
    NSInteger _selectedIndex;
    DataModel *_dataModel;
}

@property (weak, nonatomic) IBOutlet UILabel *myCarLabel;
@property (weak, nonatomic) IBOutlet UIImageView *hillImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cheapNewCarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cheapNewCarShadowImageView;
@property (weak, nonatomic) IBOutlet StarEmitterView *cheapNewCarEmitterView;
@property (weak, nonatomic) IBOutlet StarEmitterView *expensiveNewCarEmitterView;
@property (weak, nonatomic) IBOutlet UIImageView *cheapOldCarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cheapOldCarShadowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *expensiveNewCarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *expensiveOldCarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *expensiveNewCarShadowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *expensiveOldCarShadowImageView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end

@implementation MyCarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
 
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.hillImageView startGIF];
    [self.cheapNewCarImageView startGIF];
    [self.cheapOldCarImageView startGIF];
    [self.expensiveNewCarImageView startGIF];
    [self.expensiveOldCarImageView startGIF];
    
    [self.cheapNewCarEmitterView start];
    [self.expensiveNewCarEmitterView start];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.hillImageView stopGIF];
    [self.cheapNewCarImageView stopGIF];
    [self.cheapOldCarImageView stopGIF];
    [self.expensiveNewCarImageView stopGIF];
    [self.expensiveOldCarImageView stopGIF];
    
    [self.cheapNewCarEmitterView stop];
    [self.expensiveNewCarEmitterView stop];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataModel = [DataModel defaultModel];
    self.myCarLabel.text = [_dataModel localizedStringForKey:@"MY_VEHICLE"];
    
    self.hillImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"hill" ofType:@"gif"];
    self.cheapNewCarImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"cheapNewCar" ofType:@"gif"];
    [self.cheapNewCarImageView loadImageWithIndex:0];
    self.cheapOldCarImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"cheapOldCar" ofType:@"gif"];
    [self.cheapOldCarImageView loadImageWithIndex:0];
    self.expensiveNewCarImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"expensiveNewCar" ofType:@"gif"];
    [self.expensiveNewCarImageView loadImageWithIndex:0];
    self.expensiveOldCarImageView.gifPath = [[NSBundle mainBundle] pathForResource:@"expensiveOldCar" ofType:@"gif"];
    [self.expensiveOldCarImageView loadImageWithIndex:0];
    
    UITapGestureRecognizer *cheapNewCarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    cheapNewCarTap.numberOfTapsRequired = 1;
    [self.cheapNewCarImageView addGestureRecognizer:cheapNewCarTap];
    UITapGestureRecognizer *cheapOldCarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    cheapOldCarTap.numberOfTapsRequired = 1;
    [self.cheapOldCarImageView addGestureRecognizer:cheapOldCarTap];
    UITapGestureRecognizer *expensiveNewCarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    expensiveNewCarTap.numberOfTapsRequired = 1;
    [self.expensiveNewCarImageView addGestureRecognizer:expensiveNewCarTap];
    UITapGestureRecognizer *expensiveOldCarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    expensiveOldCarTap.numberOfTapsRequired = 1;
    [self.expensiveOldCarImageView addGestureRecognizer:expensiveOldCarTap];
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.nextBtn addGestureRecognizer:pan];
    
    _selectedIndex = -1;
    if ([_dataModel getCarSelection]) {
        [self selectItemWithIndex:[[_dataModel getCarSelection] integerValue]];
    }
}

- (void)selectItemWithIndex:(NSInteger)index
{
    switch (index) {
        case 1:{
            if (_selectedIndex == 1) {
                self.cheapNewCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                _selectedIndex = -1;
            }else{
                self.cheapNewCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_selected"];
                self.cheapOldCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                self.expensiveNewCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                self.expensiveOldCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                _selectedIndex = 1;
            }
        }
            break;
        case 2:{
            if (_selectedIndex == 2) {
                self.cheapOldCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                _selectedIndex = -1;
            }else{
                self.cheapOldCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_selected"];
                self.cheapNewCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                self.expensiveNewCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                self.expensiveOldCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                _selectedIndex = 2;
            }
        }
            break;
        case 3:{
            if (_selectedIndex == 3) {
                self.expensiveNewCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                _selectedIndex = -1;
            }else{
                self.expensiveNewCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_selected"];
                self.cheapNewCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                self.cheapOldCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                self.expensiveOldCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                _selectedIndex = 3;
            }
        }
            break;
        case 4:{
            if (_selectedIndex == 4) {
                self.expensiveOldCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                _selectedIndex = -1;
            }else{
                self.expensiveOldCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_selected"];
                self.expensiveNewCarShadowImageView.image = [UIImage imageNamed:@"expensiveWheel_unselected"];
                self.cheapNewCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                self.cheapOldCarShadowImageView.image = [UIImage imageNamed:@"cheapWheel_unselected"];
                _selectedIndex = 4;
            }
        }
            break;
        default:
            return;
            break;
    }
}

#pragma mark -
#pragma mark Gesture

- (void)handleTapGesture:(UITapGestureRecognizer *)tap
{
    [self selectItemWithIndex:tap.view.tag];
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)pan
{
    UIView *piece = [pan view];
    
    [self adjustAnchorPointForGestureRecognizer:pan];
    
    if ([pan state] == UIGestureRecognizerStateBegan || [pan state] == UIGestureRecognizerStateChanged) {
        CGPoint translation = [pan translationInView:[piece superview]];
        
        float x = [piece center].x + translation.x;
        float y = [piece center].y + translation.y;
        
        if (x < CGRectGetWidth(piece.frame) / 2) {
            x = CGRectGetWidth(piece.frame) / 2;
        }else if (x > (CGRectGetWidth(self.view.frame) - CGRectGetWidth(piece.frame)/2)){
            x = CGRectGetWidth(self.view.frame) - CGRectGetWidth(piece.frame)/2;
        }
        
        if (y < CGRectGetHeight(piece.frame) / 2) {
            y = CGRectGetHeight(piece.frame) / 2;
        }else if (y > (CGRectGetHeight(self.view.frame) - CGRectGetHeight(piece.frame)/2)){
            y = CGRectGetHeight(self.view.frame) - CGRectGetHeight(piece.frame)/2;
        }
        
        [piece setCenter:CGPointMake( x, y)];
        [pan setTranslation:CGPointZero inView:[piece superview]];
    }
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

#pragma mark -
#pragma mark Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"nextStep"]) {
        if (_selectedIndex == -1) {
            [Utils showAlertWithMsg:WARNING_NO_SELECTION];
            return NO;
        }
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[DataModel defaultModel] setCarSelection:@(_selectedIndex)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
