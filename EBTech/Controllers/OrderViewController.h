//
//  OrderViewController.h
//  EBTech
//
//  Created by shimingwei on 14-8-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewController : UIViewController

- (void)setLocation:(NSString *)location plateNumber:(NSString*)plateNumber purchaseDate:(NSString *)purchaseDate marketValue:(NSString*)marketValue itemViews:(NSArray *)itemViews totalPremium:(NSString*)totalPremium;

@end
