//
//  AbbreviationPickerViewController.h
//  EBTech
//
//  Created by shimingwei on 14-8-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AbbreviationPickerViewController;
@protocol AbbreviationPickerViewControllerdelegate <NSObject>

@required
- (void)abbreviationPickerViewControllerClickCancel;
- (void)abbreviationPickerViewControllerClickSelect:(AbbreviationPickerViewController*)abbreviationPickerViewController;

@end

@interface AbbreviationPickerViewController : UIViewController

@property (nonatomic)id<AbbreviationPickerViewControllerdelegate>delegate;
@property (nonatomic, strong) NSString *selectedAbbreviation;

- (void)setDefaultSelection:(NSString*)defaultSelection;

@end
