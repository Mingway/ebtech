//
//  CartViewController.m
//  EBTech
//
//  Created by shimingwei on 14-8-13.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "CartViewController.h"
#import "UIButton+Extensions.h"
#import "Utils.h"

@interface CartViewController ()

@end

@implementation CartViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}
 
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setSelectedItems:(NSArray *)items
{
    _items = [NSMutableArray arrayWithArray:items];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _items ? _items.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ItemCell"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 11, 76, 61)];
    [cell.contentView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icn_%@",[Utils getLossImageNameWithIndex:[_items[indexPath.row] integerValue]]]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(104, 0, 150, 84)];
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:15.0f];
    [cell.contentView addSubview:label];
    label.text = [Utils getLossTitleWithIndex:[_items[indexPath.row] integerValue]];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton setFrame:CGRectMake(258, 31, 22, 22)];
    [cell.contentView addSubview:deleteButton];
    [deleteButton setBackgroundImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    deleteButton.tag = indexPath.row;
    deleteButton.hidden = YES;
    if ([_items[indexPath.row] integerValue] > 10) {
        deleteButton.hidden = NO;
        [deleteButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
        [deleteButton addTarget:self action:@selector(deleteItem:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 83, 1024, 1)];
        lineLabel.backgroundColor = [UIColor colorWithRed:229/255.0f green:227/255.0f blue:224/255.0f alpha:1.0f];
        [cell addSubview:lineLabel];
        
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 83, 1024, 1)];
        lineLabel.backgroundColor = [UIColor whiteColor];
        [cell addSubview:lineLabel];
        
        cell.backgroundColor = [UIColor colorWithRed:246/255.0f green:246/255.0f blue:246/255.0f alpha:1.0f];
    }
    
    return cell;
}

- (void)deleteItem:(UIButton *)btn
{
    if (_delegate && [_delegate respondsToSelector:@selector(deleteItemViewWithIndex:)]) {
        [_delegate deleteItemViewWithIndex:[_items[btn.tag] integerValue]];
    }
    [_items removeObjectAtIndex:btn.tag];
    [self.tableView reloadData];
}

@end
