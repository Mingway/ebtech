//
//  LossItemView.h
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LossItemView;
@protocol LossItemViewDelegate <NSObject>

@optional
- (void)tapItemView:(LossItemView *)itemView;

@end

NS_ENUM(NSInteger, ItemType){
    Basic = 1,
    Optional
};

@interface LossItemView : UIView

@property (nonatomic)id<LossItemViewDelegate> delegate;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView *insuranceIconImageView;
@property (nonatomic, assign) enum ItemType type;
@property (nonatomic, assign) NSInteger index;

- (instancetype) initWithFrame:(CGRect)frame index:(NSInteger)index title:(NSString *)title imageName:(NSString *)imageName type:(enum ItemType)type delegate:(id<LossItemViewDelegate>)delegate;

- (void)addInsuranceIconWithAnimation:(BOOL)animation completion:(void (^)(BOOL finished))completion;
- (void)removeInsuranceIconImageView;

@end
