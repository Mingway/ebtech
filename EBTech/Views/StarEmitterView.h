//
//  StarEmitterView.h
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarEmitterView : UIView

- (void)start;
- (void)stop;

@end
