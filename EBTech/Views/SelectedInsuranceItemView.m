//
//  SelectedInsuranceItemView.m
//  EBTech
//
//  Created by shimingwei on 14-8-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "SelectedInsuranceItemView.h"
#import "DataModel.h"

@interface SelectedInsuranceItemView ()<UITextFieldDelegate>{
    NSArray *_choiceArr;
    float _factor;
    DataModel *_dataModel;
}

@end

@implementation SelectedInsuranceItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame titleStr:(NSString *)titleStr purchaseIndex:(NSString *)purchaseIndex iconImageName:(NSString *)iconImageName type:(ItemType)type choiceArr:(NSArray*)choiceArr defaultValue:(float)defualtValue factor:(float)factor
{
    self = [super initWithFrame:frame];
    if (self) {
        _dataModel = [DataModel defaultModel];
        
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor orangeColor];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(10, 4, 455, 192)];
        bgView.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];
        [self addSubview:bgView];
        
        CGSize size = CGSizeMake(420, 10000);
        
        CGRect rect = [titleStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:20.0f]} context:nil];
        if (!_titleLabel) {
            _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, rect.size.width, rect.size.height)];
            _titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
            _titleLabel.text = titleStr;
            [self addSubview:_titleLabel];
        }
        
        UIImageView *lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_titleLabel.frame) + 20, 350, 4)];
        lineImageView.image = [UIImage imageNamed:@"grayLine"];
        [self addSubview:lineImageView];
        
        NSString *purchaseIndexStr = [NSString stringWithFormat:[_dataModel localizedStringForKey:@"PURCHASE"],purchaseIndex];
        rect = [purchaseIndexStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0f]} context:nil];
        
        if (!_purchaseIndexLabel) {
            _purchaseIndexLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_titleLabel.frame) + 10, 36, rect.size.width, rect.size.height)];
            _purchaseIndexLabel.textColor = [UIColor orangeColor];
            _purchaseIndexLabel.font = [UIFont boldSystemFontOfSize:16.0f];
            _purchaseIndexLabel.text = purchaseIndexStr;
            [self addSubview:_purchaseIndexLabel];
        }
        
        UIImageView *dropListBGImaegView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 90, 279, 54)];
        switch (type) {
            case Input:
                dropListBGImaegView.image = [UIImage imageNamed:@"smallBlueDropList"];
                break;
            case Default:
                dropListBGImaegView.image = [UIImage imageNamed:@"smallGrayDropList"];
                break;
            case Choice:
                dropListBGImaegView.image = [UIImage imageNamed:@"smallBlueDropList"];
                break;
            default:
                break;
        }
        dropListBGImaegView.userInteractionEnabled = YES;
        [self addSubview:dropListBGImaegView];
        
        UILabel *insuredTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 74, 54)];
        insuredTitleLabel.textAlignment = NSTextAlignmentCenter;
        insuredTitleLabel.text = [_dataModel localizedStringForKey:@"LIMIT"];
        if (type == Choice || type == Input) {
            insuredTitleLabel.textColor = [UIColor colorWithRed:42/255.0f green:113/255.0f blue:200/255.0f alpha:1.0f];
        }else{
            insuredTitleLabel.textColor = [UIColor grayColor];
        }
        
        insuredTitleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        [dropListBGImaegView addSubview:insuredTitleLabel];
        
        if (!_dropListBtn) {
            _dropListBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        }
        [_dropListBtn setFrame:CGRectMake(75, 0, 204, 54)];
        _dropListBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 150, 0, 0);
        switch (type) {
            case Input:
                break;
            case Default:
                break;
            case Choice:
                [_dropListBtn setImage:[UIImage imageNamed:@"dropListBtn"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        [dropListBGImaegView addSubview:_dropListBtn];
        /*
        UILabel *symbolLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 160, 40, 30)];
        symbolLabel.text = @"¥";
        symbolLabel.font = [UIFont boldSystemFontOfSize:30.0f];
        symbolLabel.textColor = [UIColor orangeColor];
        symbolLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:symbolLabel];
         */
        
        if (!_priceLabel) {
            _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 160, 420, 30)];
            _priceLabel.textColor = [UIColor orangeColor];
            _priceLabel.font = [UIFont boldSystemFontOfSize:30.0f];
            _priceLabel.hidden = YES;
            [self addSubview:_priceLabel];
        }
        
        self.type = type;
        switch (self.type) {
            case Input:{
                if (!_limitTextField) {
                    _limitTextField = [[UITextField alloc] initWithFrame:CGRectMake(80, 0, 140, 54)];
                    _limitTextField.borderStyle = UITextBorderStyleNone;
                    _limitTextField.delegate = self;
                    _limitTextField.keyboardType = UIKeyboardTypeNumberPad;
                    [dropListBGImaegView addSubview:_limitTextField];
                    _limitTextField.text = [NSString stringWithFormat:@"%.2f",defualtValue];
                }
            }
                break;
            case Default:{
                if (!_limitLabel) {
                    _limitLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 140, 54)];
                    _limitLabel.text = [NSString stringWithFormat:@"%.2f",defualtValue];
                    _limitLabel.textColor = [UIColor grayColor];
                    [dropListBGImaegView addSubview:_limitLabel];
                }
                _priceLabel.text = [NSString stringWithFormat:@"%.2f",defualtValue];
            }
                break;
            case Choice:{
                _choiceArr = choiceArr;
                if (!_limitLabel) {
                    _limitLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 140, 54)];
                    if (choiceArr.count > 0) {
                        _limitLabel.text = [NSString stringWithFormat:@"%@",choiceArr[0]];
                        _priceLabel.text = [NSString stringWithFormat:@"%.2f",[choiceArr[0] floatValue] * factor];
                    }
                    [dropListBGImaegView addSubview:_limitLabel];
                }
                [_dropListBtn addTarget:self action:@selector(showDropListView) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                break;
        } 
        
        _factor = factor;
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(390, 40, 50, 50)];
        iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        iconImageView.image = [UIImage imageNamed:iconImageName];
        [self addSubview:iconImageView];
        
        self.choiceArr = choiceArr;
    }
    return self;
}

- (void)showDropListView
{
    if (_delegate && [_delegate respondsToSelector:@selector(selectedValuesForSelectedInsuranceItemView:)]) {
        [_delegate selectedValuesForSelectedInsuranceItemView:self];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    _priceLabel.text = [NSString stringWithFormat:@"%.2f",[textField.text floatValue]  * _factor];
    return YES;
}

- (void)setLimit:(NSString *)limit
{
    _limitLabel.text = [NSString stringWithFormat:@"%@",limit];
    _priceLabel.text = [NSString stringWithFormat:@"%.2f",[limit floatValue] * _factor];

}
 
@end
