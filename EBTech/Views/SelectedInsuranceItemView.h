//
//  SelectedInsuranceItemView.h
//  EBTech
//
//  Created by shimingwei on 14-8-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlanFactor;
typedef enum {
    Input = 1,
    Default,
    Choice
}ItemType;

@class SelectedInsuranceItemView;
@protocol SelectedInsuranceItemViewDelegate <NSObject>

- (void)selectedValuesForSelectedInsuranceItemView:(SelectedInsuranceItemView*)selectedInsuranceItemView;

@end

@interface SelectedInsuranceItemView : UIView

@property (nonatomic) id<SelectedInsuranceItemViewDelegate> delegate;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *purchaseIndexLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, assign) ItemType type;
@property (nonatomic, strong) UITextField *limitTextField;
@property (nonatomic, strong) UILabel *limitLabel;
@property (nonatomic, strong) NSArray *choiceArr;
@property (nonatomic, strong) UIButton *dropListBtn;

@property (nonatomic, strong) PlanFactor *planFactor;

- (instancetype) initWithFrame:(CGRect)frame titleStr:(NSString *)titleStr purchaseIndex:(NSString *)purchaseIndex iconImageName:(NSString *)iconImageName type:(ItemType)type choiceArr:(NSArray*)choiceArr defaultValue:(float)defualtValue factor:(float)factor;
- (void)setLimit:(NSString *)limit;

@end
