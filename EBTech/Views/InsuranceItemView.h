//
//  InsuranceItemView.h
//  EBTech
//
//  Created by shimingwei on 14-8-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InsuranceItemView;
@protocol InsuranceItemViewDelegate <NSObject>

- (void)insuranceViewClickButton:(InsuranceItemView*)itemView;

@end

@interface InsuranceItemView : UIView

@property (nonatomic)id<InsuranceItemViewDelegate>delegate;
@property (nonatomic, strong) NSString *coverageStr;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *coverageTitleView;
@property (nonatomic, strong) UIView *coverageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *purchaseIndexLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, assign) NSInteger index;

- (instancetype) initWithFrame:(CGRect)frame coverageStr:(NSString *)coverageStr titleStr:(NSString *)titleStr purchaseIndex:(NSString *)purchaseIndex iconImageName:(NSString *)iconImageName index:(NSInteger)index delegate:(id<InsuranceItemViewDelegate>)delegate;

- (void)showOrHide;

@end
