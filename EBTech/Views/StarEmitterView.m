//
//  StarEmitterView.m
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "StarEmitterView.h"

@import QuartzCore;

@interface StarEmitterView ()
@property (retain) CAEmitterLayer *snowLayer;

@end

@implementation StarEmitterView

-(id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
        [self prepare];
	}
	
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.backgroundColor = [UIColor clearColor];
        [self prepare];
	}
	
	return self;
}

- (void)dealloc
{
    _snowLayer = nil;
}

+ (Class) layerClass {
    return [CAEmitterLayer class];
}

- (void)prepare
{
    CAEmitterLayer *snowLayer = [self makeEmitter];
    CAEmitterCell *snowCell1 = [self makeEmitterCellWithParticle:@"star"];
    [snowLayer setEmitterCells:@[snowCell1]];
    [self setSnowLayer:snowLayer];
}

- (CAEmitterLayer *)makeEmitter{
    CAEmitterLayer *emitterLayer = [CAEmitterLayer layer];
	emitterLayer.name = @"starLayer";
    //粒子发射位置
	emitterLayer.emitterPosition = CGPointMake(CGRectGetMidX([self bounds]), 0);
	emitterLayer.emitterZPosition = 0;
    //发射源的尺寸大小
	emitterLayer.emitterSize = CGSizeMake([self bounds].size.width, [self bounds].size.width);
	emitterLayer.emitterDepth = 0.00;
    //发射源的形状
	emitterLayer.emitterShape = kCAEmitterLayerCircle;
    //发射模式
	emitterLayer.emitterMode = kCAEmitterLayerSurface;
	emitterLayer.renderMode = kCAEmitterLayerBackToFront;
	emitterLayer.seed = 721963909;
    return emitterLayer;
}

- (CAEmitterCell *)makeEmitterCellWithParticle:(NSString *)name
{
	CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
	
	emitterCell.name = name;
	emitterCell.enabled = YES;
    
	emitterCell.contents = (id)[[UIImage imageNamed:name] CGImage];
	emitterCell.contentsRect = CGRectMake(0.00, 0.00, 1.00, 1.00);
    
	emitterCell.magnificationFilter = kCAFilterTrilinear;
	emitterCell.minificationFilter = kCAFilterLinear;
	emitterCell.minificationFilterBias = 0.00;
    
	emitterCell.scale = 0.72;
	emitterCell.scaleRange = 0.14;
	emitterCell.scaleSpeed = -0.25;
    
	emitterCell.color = [[UIColor whiteColor] CGColor];
    /*
	emitterCell.redRange = 0.9;
	emitterCell.greenRange = 0.8;
	emitterCell.blueRange = 0.7;
	emitterCell.alphaRange = 0.8;
    
	emitterCell.redSpeed = 0.92;
	emitterCell.greenSpeed = 0.84;
	emitterCell.blueSpeed = 0.74;
     */
	emitterCell.alphaSpeed = 0.55;
    
    //粒子参数的速度乘数因子
	emitterCell.birthRate = 0;
    emitterCell.lifetime = 0.2;
	emitterCell.lifetimeRange = 1.37;
    //粒子速度
	emitterCell.velocity = -40.00;
    //粒子的速度范围
	emitterCell.velocityRange = 2.00;
    //粒子x方向的加速度分量
	emitterCell.xAcceleration = 1.00;
    //粒子y方向的加速度分量
	emitterCell.yAcceleration = 10.00;
    //粒子z方向的加速度分量
	emitterCell.zAcceleration = 12.00;
    
    //子旋转角度范围
	emitterCell.spin = 0.384;
	emitterCell.spinRange = 0.925;
	emitterCell.emissionLatitude = 1.745;
	emitterCell.emissionLongitude = 1.745;
    //周围发射角度
	emitterCell.emissionRange = 3.491;
    
    return emitterCell;
}

- (void)start
{
    [[self snowLayer] setValue:@20 forKeyPath:@"emitterCells.star.birthRate"];
    [[self layer] addSublayer:[self snowLayer]];
}

- (void)stop
{
    [[self snowLayer] setValue:@0 forKeyPath:@"emitterCells.star.birthRate"];
    [[self snowLayer] removeFromSuperlayer];
}

@end
