//
//  InsuranceItemView.m
//  EBTech
//
//  Created by shimingwei on 14-8-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "InsuranceItemView.h"
#import "DataModel.h"

@interface InsuranceItemView (){
    DataModel *_dataModel;
}

@end

@implementation InsuranceItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame coverageStr:(NSString *)coverageStr titleStr:(NSString *)titleStr purchaseIndex:(NSString *)purchaseIndex iconImageName:(NSString *)iconImageName index:(NSInteger)index delegate:(id<InsuranceItemViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        _dataModel = [DataModel defaultModel];
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:0.8f];
        
        if (!_bgView) {
            _bgView = [[UIView alloc] initWithFrame:CGRectMake(10, 4, 455, 142)];
            _bgView.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];
            [self addSubview:_bgView];
        }
        
        CGSize size = CGSizeMake(420, 10000);
        CGRect rect = [coverageStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil];
        
        if (!_coverageView) {
            _coverageView = [[UIView alloc] initWithFrame:CGRectMake(20, 100, 420, rect.size.height)];
            
            UILabel *coverageLabel = [[UILabel alloc] initWithFrame:_coverageView.bounds];
            coverageLabel.font = [UIFont systemFontOfSize:14.0f];
            coverageLabel.text = coverageStr;
            [self addSubview:coverageLabel];
            coverageLabel.numberOfLines = 0;
            [_coverageView addSubview:coverageLabel];
            _coverageView.hidden = YES;
            [self addSubview:_coverageView];
        }
        
        if (!_coverageTitleView) {
            _coverageTitleView = [[UIView alloc] initWithFrame:CGRectMake(20, 100, 420, 30)];
            
            UIImageView *umbrellaImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 4, 22, 22)];
            umbrellaImageView.image = [UIImage imageNamed:@"smallUmbrella"];
            UILabel *coverageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 30)];
            coverageTitleLabel.textAlignment = NSTextAlignmentLeft;
            coverageTitleLabel.textColor = [UIColor colorWithRed:118/255.0f green:191/255.0f blue:246/255.0f alpha:1.0f];
            coverageTitleLabel.text = [_dataModel localizedStringForKey:@"COVERD"];
            coverageTitleLabel.font = [UIFont boldSystemFontOfSize:18.f];
            [_coverageTitleView addSubview:coverageTitleLabel];
            [_coverageTitleView addSubview:umbrellaImageView];
            [self addSubview:_coverageTitleView];
        }
        
        rect = [titleStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:20.0f]} context:nil];
        if (!_titleLabel) {
            _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, rect.size.width, rect.size.height)];
            _titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
            _titleLabel.text = titleStr;
            [self addSubview:_titleLabel];
        }
        
        UIImageView *lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_titleLabel.frame) + 20, 350, 4)];
        lineImageView.image = [UIImage imageNamed:@"grayLine"];
        [self addSubview:lineImageView];
        
        NSString *purchaseIndexStr = [NSString stringWithFormat:[_dataModel localizedStringForKey:@"PURCHASE"],purchaseIndex];
        rect = [purchaseIndexStr boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0f]} context:nil];
        
        if (!_purchaseIndexLabel) {
            _purchaseIndexLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_titleLabel.frame) + 10, 36, rect.size.width, rect.size.height)];
            _purchaseIndexLabel.textColor = [UIColor orangeColor];
            _purchaseIndexLabel.font = [UIFont boldSystemFontOfSize:16.0f];
            _purchaseIndexLabel.text = purchaseIndexStr;
            [self addSubview:_purchaseIndexLabel];
        }
        self.delegate = delegate;
        self.coverageStr = coverageStr;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOrHide)];
        [self addGestureRecognizer:tap];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(390, 40, 50, 50)];
        iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        iconImageView.image = [UIImage imageNamed:iconImageName];
        [self addSubview:iconImageView];
        
        self.index = index;
    }
    return self;
}

- (void)showOrHide
{
    self.isOpen = !self.isOpen;
    if (_delegate && [_delegate respondsToSelector:@selector(insuranceViewClickButton:)]) {
        [_delegate insuranceViewClickButton:self];
    }
    
    if (self.isOpen) {
        self.backgroundColor = [UIColor orangeColor];
        _coverageTitleView.hidden = YES;
        _coverageView.hidden = NO;
    }else{
        self.backgroundColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:0.8f];
        _coverageTitleView.hidden = NO;
        _coverageView.hidden = YES;
    }
}

@end
