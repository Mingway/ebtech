//
//  LossItemView.m
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "LossItemView.h"
#import "UIImageView+PlayGIF.h"

@implementation LossItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame index:(NSInteger)index title:(NSString *)title imageName:(NSString *)imageName type:(enum ItemType)type delegate:(id<LossItemViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        self.type = type;
        if (!_iconImageView) {
            _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 125, 100)];
            _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
            _iconImageView.userInteractionEnabled = YES;
            [self addSubview:_iconImageView];
            if (type == Optional) {
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
                [self addGestureRecognizer:tap];
            }
        }
        if (!_textLabel) {
            _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 125, 50)];
            _textLabel.textAlignment = NSTextAlignmentCenter;
            _textLabel.numberOfLines = 0;
            _textLabel.font = [UIFont boldSystemFontOfSize:14.0f];
            [self addSubview:_textLabel];
        }
        _textLabel.text = title;
        if (self.type == Basic) {
            _iconImageView.image = [UIImage imageNamed:imageName];
            [self addInsuranceIconWithAnimation:NO completion:nil];
        }else{
            _iconImageView.gifPath = [[NSBundle mainBundle] pathForResource:imageName ofType:@"gif"];
        }
        self.backgroundColor = [UIColor clearColor];
        self.index = index;
        self.delegate = delegate;
    }
    return self;
}

- (void)addInsuranceIconWithAnimation:(BOOL)animation completion:(void (^)(BOOL finished))completion
{
    if (!_insuranceIconImageView) {
        _insuranceIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(80, 50, 68, 63)];
        _insuranceIconImageView.image = [UIImage imageNamed:@"insurance"];
    }
    [self addSubview:_insuranceIconImageView];
    
    if (animation) {
        _insuranceIconImageView.transform = CGAffineTransformMakeScale(2.0f, 2.0f);
        [UIView animateWithDuration:0.3f animations:^{
            _insuranceIconImageView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            completion(YES);
        }];
    }
}

- (void)tap:(UITapGestureRecognizer *)tap
{
    [self addInsuranceIconWithAnimation:YES completion:^(BOOL finished) {
        if (_delegate && [_delegate respondsToSelector:@selector(tapItemView:)]) {
            [_delegate tapItemView:self];
        }
    }];
}

- (void)removeInsuranceIconImageView
{
    if (_insuranceIconImageView) {
        [_insuranceIconImageView removeFromSuperview];
        _insuranceIconImageView = nil;
    }
}

@end
