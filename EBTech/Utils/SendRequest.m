//
//  SendRequest.m
//  EBTech
//
//  Created by shimingwei on 14-8-26.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "SendRequest.h"
#import "RTSpinKitView.h"

@interface SendRequest (){
    RTSpinKitView *_spinKitView;
}

@end

@implementation SendRequest

- (void)dealloc
{
    if (_spinKitView) {
        [_spinKitView removeFromSuperview];
        _spinKitView = nil;
    }
}

+ (NSOperationQueue *)getQueue
{
    static NSOperationQueue *queue = nil;
    if (!queue) {
        queue = [[NSOperationQueue alloc] init];
    }
    return queue;
}

- (void)sendAsyncRequestWithCompletionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler
{
    if (!_spinKitView) {
        _spinKitView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:[UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.0]];
    }
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    _spinKitView.center = CGPointMake(CGRectGetMidY(screenBounds), CGRectGetMidX(screenBounds));
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:_spinKitView];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.url]];
    [request setHTTPMethod:@"POST"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSString *HTTPBodyString = self.httpBody;
    [request setHTTPBody:[HTTPBodyString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[SendRequest getQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        [_spinKitView removeFromSuperview];
        _spinKitView = nil;
        NSString *errorMsg;
        BOOL isSuccess = NO;
        if (connectionError) {
            NSLog(@"%@",connectionError.description);
            errorMsg = connectionError.description;
        }else{
            NSError *error;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            if (error) {
                NSLog(@"%@",error.description);
                errorMsg = error.description;
            }else{
                NSLog(@"%@",result);
                isSuccess = YES;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(isSuccess,result,errorMsg);
            });
        }
    }];
}

@end
