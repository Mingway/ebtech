//
//  Utils.m
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "Utils.h"
#import "DataModel.h"

@implementation Utils

+ (void)showAlertWithMsg:(NSString *)msg
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}

+ (NSString *)getLossTitleWithIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            return [[DataModel defaultModel] localizedStringForKey:@"LOSS_OF_VEHICLE"];
            break;
        case 2:
            return [[DataModel defaultModel] localizedStringForKey:@"REPAIRING_COSTS"];
            break;
        case 3:
            return [[DataModel defaultModel] localizedStringForKey:@"ROADSIDE_ASSISTANCE"];
            break;
        case 4:
            return [[DataModel defaultModel] localizedStringForKey:@"ACCESSORIES_PROTECTION"];
            break;
        case 5:
            return [[DataModel defaultModel] localizedStringForKey:@"TRANSPORTATION_EXPENSES"];
            break;
        case 11:
            return [[DataModel defaultModel] localizedStringForKey:@"WINDSCREEN_REPAIR"];
            break;
        case 12:
            return [[DataModel defaultModel] localizedStringForKey:@"DRIVER_ACCIDENT"];
            break;
        case 13:
            return [[DataModel defaultModel] localizedStringForKey:@"PASSANGER_ACCIDENT"];
            break;
        case 14:
            return [[DataModel defaultModel] localizedStringForKey:@"THIRD_INJURY"];
            break;
        case 15:
            return [[DataModel defaultModel] localizedStringForKey:@"THIRD_DAMAGE"];
            break;
        case 16:
            return [[DataModel defaultModel] localizedStringForKey:@"CAR_SCRATCH"];
            break;
        case 17:
            return [[DataModel defaultModel] localizedStringForKey:@"MORAL_DAMAGE"];
            break;
        default:
            return @"";
            break;
    }
}

+ (NSString *)getLossImageNameWithIndex:(NSInteger)index
{
    switch (index) {
        case 1:
            return @"lossOfVehicles";
            break;
        case 2:
            return @"maintenanceCosts";
            break;
        case 3:
            return @"medicalExpenses";
            break;
        case 4:
            return @"lossOfEquipment";
            break;
        case 5:
            return @"travelExpenses";
            break;
        case 11:
            return @"windshieldLoss";
            break;
        case 12:
            return @"mainDriverInjury";
            break;
        case 13:
            return @"passengersInjury";
            break;
        case 14:
            return @"thirdPartyLiabilityInJury";
            break;
        case 15:
            return @"thirdPartyLiabilityLoss";
            break;
        case 16:
            return @"carBodyScratches";
            break;
        case 17:
            return @"mentalAnguish";
            break;
        default:
            return @"";
            break;
    }
}

+ (NSString *)getPercentWithIndex:(NSInteger)index
{
    switch (index) {
        case 21:
            return @"85%";
            break;
        case 22:
            return @"25%";
            break;
        case 23:
            return @"60%";
            break;
        case 24:
            return @"10%";
            break;
        case 25:
            return @"15%";
            break;
        case 26:
            return @"45%";
            break;
        case 27:
            return @"95%";
            break;
        case 28:
            return @"30%";
            break;
        case 29:
            return @"20%";
            break;
        case 30:
            return @"10%";
            break;
        default:
            return @"";
            break;
    }
}

@end
