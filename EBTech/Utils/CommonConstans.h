//
//  CommonConstans.h
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#ifndef EBTech_CommonConstans_h
#define EBTech_CommonConstans_h

#define WARNING_NO_SELECTION        NSLocalizedString(@"NO_SELECTION", nil)
#define WARNING_FILL_BLANK          NSLocalizedString(@"FILL_BLANK", nil)

#endif
