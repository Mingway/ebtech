//
//  GetData.m
//  EBTech
//
//  Created by shimingwei on 14-8-26.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "GetData.h"
#import "SendRequest.h"
#import "SBJson.h"
#import "CommonFunc.h"

#define PRODUCT_ID      @"RPgPmsjJsA:1"
#define CHANNEL_ID      @(80006)

@interface GetData ()

@end

@implementation GetData

- (void)getPlanFactorsWithCompletionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler
{
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    SendRequest *sendRequest = [[SendRequest alloc] init];
    sendRequest.url = [NSString stringWithFormat:@"%@products/plan_factors?key=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"serverURL"],[CommonFunc base64StringFromText:[NSString stringWithFormat:@"%@:%@",username,password]]];
    NSDictionary *dic = @{@"product_id":PRODUCT_ID,@"channel_id":CHANNEL_ID,@"factor_table":@{@"effectiveDate":@"2014-08-26T02:44:03.880Z",@"expiredDate":@"2015-08-25T02:44:03.880Z"}};
    sendRequest.httpBody = [dic JSONRepresentation];
    
    [sendRequest sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        completionHandler(isSuccess,result,errorMessage);
    }];
}

- (void)calculateWithPlans:(NSDictionary*)planDics uuids:(NSDictionary*)uuidDics completionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler
{
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    SendRequest *sendRequest = [[SendRequest alloc] init];
    sendRequest.url = [NSString stringWithFormat:@"%@quotation?key=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"serverURL"],[CommonFunc base64StringFromText:[NSString stringWithFormat:@"%@:%@",username,password]]];
    NSDictionary *dic = @{@"product_id":PRODUCT_ID,@"channel_id":CHANNEL_ID,@"factor_table":planDics,@"selection":uuidDics,@"useNewRatingEngine":@(YES),@"campaign_ids":@[]};
    sendRequest.httpBody = [dic JSONRepresentation];
    
    [sendRequest sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        completionHandler(isSuccess,result,errorMessage);
    }];
}

- (void)saveOrderWithCalculation:(NSDictionary*)calculationDic completionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler
{
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    SendRequest *sendRequest = [[SendRequest alloc] init];
    sendRequest.url = [NSString stringWithFormat:@"%@quotation/save?key=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"serverURL"],[CommonFunc base64StringFromText:[NSString stringWithFormat:@"%@:%@",username,password]]];
    NSDictionary *dic = @{@"country":@"cn",@"productId":PRODUCT_ID,@"channelId":CHANNEL_ID,@"company":@"_ALL_PERMITTED",@"quotationId":@"",@"results":@{@"product_id":PRODUCT_ID,@"calculation":calculationDic},@"viewData":@{},@"effectiveDate":@((long)[[NSDate date] timeIntervalSinceNow]),@"expiredDate":@((long)[[NSDate date] timeIntervalSinceNow]),@"underwriting":@{@"passes":@(true)},@"success":@(true),@"plan_code":@"",@"type":@"quotation",@"ruleCheckResult":@"PASS",@"quotationStatus":@"QUOTATION_IN_PROGRESS",@"validationrule":@{@"passed":@(YES)}};
    sendRequest.httpBody = [dic JSONRepresentation];
    
    [sendRequest sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        completionHandler(isSuccess,result,errorMessage);
    }];
}

@end
