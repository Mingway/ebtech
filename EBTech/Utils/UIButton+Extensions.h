//
//  UIButton+Extensions.h
//  zxjyw
//
//  Created by HelyData on 13-10-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extensions)
@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;
@end
