//
//  GetData.h
//  EBTech
//
//  Created by shimingwei on 14-8-26.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetData : NSObject

- (void)getPlanFactorsWithCompletionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler;
- (void)calculateWithPlans:(NSDictionary*)planDics uuids:(NSDictionary*)uuidDics completionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler;
- (void)saveOrderWithCalculation:(NSDictionary*)calculationDic completionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler;

@end
