//
//  SendRequest.h
//  EBTech
//
//  Created by shimingwei on 14-8-26.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SendRequest : NSObject

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *httpBody;

- (void)sendAsyncRequestWithCompletionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler;

@end
