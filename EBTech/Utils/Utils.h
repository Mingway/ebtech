//
//  Utils.h
//  EBTech
//
//  Created by shimingwei on 14-8-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (void)showAlertWithMsg:(NSString *)msg;

+ (NSString *)getLossTitleWithIndex:(NSInteger)index;
+ (NSString *)getLossImageNameWithIndex:(NSInteger)index;
+ (NSString *)getPercentWithIndex:(NSInteger)index;

@end
