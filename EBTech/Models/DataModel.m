//
//  DataModel.m
//  EBTech
//
//  Created by shimingwei on 14-8-13.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "DataModel.h"

@interface DataModel ()

@property (nonatomic, strong) NSNumber *carSelection;
@property (nonatomic, strong) NSArray *carAccidents;
@property (nonatomic, strong) NSArray *loss;
@property (nonatomic, strong) NSArray *insurance;

@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *dateStr;
@property (nonatomic, strong) NSString *abbreviation;
@property (nonatomic, strong) NSString *plateNumber;
@property (nonatomic, strong) NSString *price;

@end

@implementation DataModel

+ (instancetype)defaultModel
{
    static DataModel *model = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[DataModel alloc] init];
    });
    return model;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        _language = [userDefault objectForKey:@"language"];
        _carSelection = [userDefault objectForKey:@"car_selection"];
        _carAccidents = [userDefault objectForKey:@"car_accidents"];
        _loss = [userDefault objectForKey:@"loss"];
        _insurance = [userDefault objectForKey:@"insurance"];
        
        _state = [userDefault objectForKey:@"state"];
        _city = [userDefault objectForKey:@"city"];
        _dateStr = [userDefault objectForKey:@"dateStr"];
        _abbreviation = [userDefault objectForKey:@"abbreviation"];
        _plateNumber = [userDefault objectForKey:@"plateNumber"];
        _price = [userDefault objectForKey:@"price"];
    }
    return self;
}

- (void)setCarSelection:(NSNumber*)selection
{
    _carSelection = selection;
    [[NSUserDefaults standardUserDefaults] setObject:selection forKey:@"car_selection"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setCarAccidents:(NSArray *)selections
{
    _carAccidents = selections;
    [[NSUserDefaults standardUserDefaults] setObject:selections forKey:@"car_accidents"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setLoss:(NSArray *)selections
{
    _loss = selections;
    [[NSUserDefaults standardUserDefaults] setObject:selections forKey:@"loss"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setInsurance:(NSArray *)selections
{
    _insurance = selections;
    [[NSUserDefaults standardUserDefaults] setObject:selections forKey:@"insurance"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setState:(NSString*)state city:(NSString*)city dateStr:(NSString*)dateStr abbreviation:(NSString*)abbreviation plateNumber:(NSString*)plateNumber price:(NSString*)price
{
    _state = state;
    _city = city;
    _dateStr = dateStr;
    _abbreviation = abbreviation;
    _plateNumber = plateNumber;
    _price = price;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:state forKey:@"state"];
    [userDefault setObject:city forKey:@"city"];
    [userDefault setObject:dateStr forKey:@"dateStr"];
    [userDefault setObject:abbreviation forKey:@"abbreviation"];
    [userDefault setObject:plateNumber forKey:@"plateNumber"];
    [userDefault setObject:price forKey:@"price"];
    [userDefault synchronize];
}

- (NSNumber *)getCarSelection
{
    return _carSelection;
}

- (NSArray *)getCarAccidents
{
    return _carAccidents;
}

- (NSArray *)getLoss
{
    return _loss;
}

- (NSArray *)getInsurance
{
    return _insurance;
}

- (NSString *)getState
{
    return _state;
}

- (NSString *)getCity

{
    return _city;
}

- (NSString *)getDateStr
{
    return _dateStr;
}

- (NSString *)getAbbreviaiton
{
    return _abbreviation;
}

- (NSString *)getPlateNumber
{
    return _plateNumber;
}

- (NSString *)getPrice
{
    return _price;
}

- (NSString*)localizedStringForKey:(NSString*)key
{
    if ([_language isEqualToNumber:@(0)]) {
        //中文
        if ([key isEqualToString:@"MY_VEHICLE"]) {
            return @"我的爱车";
        }else if([key isEqualToString:@"AFRAID"]){
            return @"我担心承担...";
        }else if ([key isEqualToString:@"AFRAID_CAR"]){
            return @"我担心爱车...";
        }else if ([key isEqualToString:@"FALLING_OBJECTS"]){
            return @"被砸";
        }else if ([key isEqualToString:@"NATURAL_DISASTER"]){
            return @"遭遇灾害";
        }else if ([key isEqualToString:@"COLLISION"]){
            return @"被撞";
        }else if ([key isEqualToString:@"SELF_IGNITION"]){
            return @"自燃";
        }else if ([key isEqualToString:@"THEFT"]){
            return @"被盗抢";
        }else if([key isEqualToString:@"LOSS_OF_VEHICLE"]){
            return @"车辆损失";
        }else if ([key isEqualToString:@"REPAIRING_COSTS"]){
            return @"维修费用";
        }else if ([key isEqualToString:@"ROADSIDE_ASSISTANCE"]){
            return @"救护费用";
        }else if ([key isEqualToString:@"ACCESSORIES_PROTECTION"]){
            return @"设备损失";
        }else if ([key isEqualToString:@"TRANSPORTATION_EXPENSES"]){
            return @"代步费用";
        }else if ([key isEqualToString:@"WINDSCREEN_REPAIR"]){
            return @"挡风玻璃损失";
        }else if ([key isEqualToString:@"DRIVER_ACCIDENT"]){
            return @"主驾人伤";
        }else if ([key isEqualToString:@"PASSANGER_ACCIDENT"]){
            return @"乘客人伤";
        }else if ([key isEqualToString:@"THIRD_INJURY"]){
            return @"三责人伤";
        }else if ([key isEqualToString:@"THIRD_DAMAGE"]){
            return @"三责物损";
        }else if ([key isEqualToString:@"CAR_SCRATCH"]){
            return @"车身划伤";
        }else if ([key isEqualToString:@"MORAL_DAMAGE"]){
            return @"精神损失";
        }else if ([key isEqualToString:@"OWN_DAMAGE"]){
            return @"车辆损失险";
        }else if ([key isEqualToString:@"FIXTURE_ACCESSORIES"]){
            return @"设备损失险";
        }else if ([key isEqualToString:@"TRANSPORT_SERVICE"]){
            return @"代步机动车服务特约条款";
        }else if ([key isEqualToString:@"SELF_IGNITION_INSURANCE"]){
            return @"自燃损失险";
        }else if ([key isEqualToString:@"ROBBERY_THEFT"]){
            return @"全车盗抢险";
        }else if ([key isEqualToString:@"PEOPLE_LIABILITY"]){
            return @"车上人员责任险";
        }else if ([key isEqualToString:@"THIRD_LIABILITY"]){
            return @"第三者责任险";
        }else if ([key isEqualToString:@"SCRATCH"]){
            return @"车身划痕损失险";
        }else if ([key isEqualToString:@"WINDOW_BREAKAGE"]){
            return @"玻璃单独破碎险";
        }else if ([key isEqualToString:@"MORAL_DAMAGE"]){
            return @"精神损失险";
        }else if ([key isEqualToString:@"OWN_DAMAGE_CONTENT"]){
            return @"保险人依照约定负责赔偿被保险机动车过程中，因碰撞等原因造成被保险机动车的直接损失。";
        }else if ([key isEqualToString:@"FIXTURE_ACCESSORIES_CONTENT"]){
            return @"保险人按照实际损失计算赔偿因发生机动车损失保险责任范围内的事故，造成车上设备的直接损毁。";
        }else if ([key isEqualToString:@"TRANSPORT_SERVICE_CONTENT"]){
            return @"发生机动车损失保险责任范围内的事故，保险人向被保险人补偿修理期间费用，作为代步车费用。";
        }else if ([key isEqualToString:@"SELF_IGNITION_INSURANCE_CONTENT"]){
            return @"保险人赔偿被保险机动车自身原因或所载货物自身原因起火燃烧造成本车的损失。";
        }else if ([key isEqualToString:@"ROBBERY_THEFT_CONTENT"]){
            return @"保险人赔偿被保险机动车全车被盗窃、抢劫、抢夺后的全车损失或合理费用。";
        }else if ([key isEqualToString:@"PEOPLE_LIABILITY_CONTENT"]){
            return @"保险人按约定赔偿被保险人在使用被保险机动车过程中发生意外事故，致使车上人员遭受人身伤亡，依法的赔偿责任。";
        }else if ([key isEqualToString:@"THIRD_LIABILITY_CONTENT"]){
            return @"保险人按约定赔偿被保险人在使用被保险机动车过程中发生意外事故，致使第三者遭受人身伤亡或财产直接损毁，依法的赔偿责任。";
        }else if ([key isEqualToString:@"SCRATCH_CONTENT"]){
            return @"保险人按约定赔偿被保险机动车发生无明显碰撞痕迹的车身划痕损失。";
        }else if ([key isEqualToString:@"WINDOW_BREAKAGE_CONTENT"]){
            return @"保险人按约定赔偿被保险机动车挡风玻璃或车窗玻璃的单独破碎损失。";
        }else if ([key isEqualToString:@"MORAL_DAMAGE_CONTENT"]){
            return @"保险人按约定赔偿车上人员或第三者的精神损害抚慰金。";
        }else if ([key isEqualToString:@"PURCHASE"]){
            return @"购买指数%@";
        }else if ([key isEqualToString:@"SHOPPIONG_CART"]){
            return @"购物车";
        }else if ([key isEqualToString:@"COVERD"]){
            return @"保障范围";
        }else if ([key isEqualToString:@"ESTIMATED_PREMIUM"]){
            return @"预估报价";
        }else if ([key isEqualToString:@"DRIVING_AREA"]){
            return @"行驶城市";
        }else if ([key isEqualToString:@"PLATE_NUMBER"]){
            return @"车牌号码";
        }else if ([key isEqualToString:@"PURCHASE_DATE"]){
            return @"提车时间";
        }else if ([key isEqualToString:@"MARKET_VALUE"]){
            return @"汽车价格";
        }else if ([key isEqualToString:@"LIMIT"]){
            return @"保额";
        }else if ([key isEqualToString:@"PREMIUM"]){
            return @"保单总价";
        }else if ([key isEqualToString:@"ORDER_DETAIL"]){
            return @"结算单";
        }else if ([key isEqualToString:@"CONFIRM_ORDER"]){
            return @"确认订单信息";
        }else if([key isEqualToString:@"ORDER_NUMBER"]){
            return @"订单编号";
        }else if ([key isEqualToString:@"SELECTED_COVERAGE"]){
            return @"承保险种";
        }else if ([key isEqualToString:@"LIMIT_"]){
            return @"保额（元）";
        }else if ([key isEqualToString:@"COVERAGE_PREMIUM"]){
            return @"保费（元）";
        }else if ([key isEqualToString:@"TOTAL_PREMIUM"]){
            return @"保单总价：";
        }else if ([key isEqualToString:@"PERIOD_OF_INSURANCE"]){
            return @"保险时间：一年制";
        }else if ([key isEqualToString:@"CANCEL"]){
            return @"取消";
        }else if ([key isEqualToString:@"SELECT"]){
            return @"选择";
        }else if ([key isEqualToString:@"SYMBOL"]){
            return @"¥";
        }else if ([key isEqualToString:@"NO_SELECTION"]){
            return @"请至少选择一项";
        }else if ([key isEqualToString:@"FILL_BLANK"]){
            return @"请填写完整";
        }
    }else if ([_language isEqualToNumber:@(1)]){
        //英文
        if ([key isEqualToString:@"MY_VEHICLE"]) {
            return @"My Vehicle";
        }else if([key isEqualToString:@"AFRAID"]){
            return @"I need protection for ...";
        }else if ([key isEqualToString:@"AFRAID_CAR"]){
            return @"I am afraid of ...";
        }else if ([key isEqualToString:@"FALLING_OBJECTS"]){
            return @"Falling Objects";
        }else if ([key isEqualToString:@"NATURAL_DISASTER"]){
            return @"Natural Disasters";
        }else if ([key isEqualToString:@"COLLISION"]){
            return @"Collision";
        }else if ([key isEqualToString:@"SELF_IGNITION"]){
            return @"Self-Ignition";
        }else if ([key isEqualToString:@"THEFT"]){
            return @"Theft";
        }else if([key isEqualToString:@"LOSS_OF_VEHICLE"]){
            return @"Loss or Damage to Insured Vehicle";
        }else if ([key isEqualToString:@"REPAIRING_COSTS"]){
            return @"Repairing Costs";
        }else if ([key isEqualToString:@"ROADSIDE_ASSISTANCE"]){
            return @"Roadside Assistance";
        }else if ([key isEqualToString:@"ACCESSORIES_PROTECTION"]){
            return @"Accessories Protection";
        }else if ([key isEqualToString:@"TRANSPORTATION_EXPENSES"]){
            return @"Transportation Expenses";
        }else if ([key isEqualToString:@"WINDSCREEN_REPAIR"]){
            return @"Windscreen Repair";
        }else if ([key isEqualToString:@"DRIVER_ACCIDENT"]){
            return @"Personal Accident to Driver";
        }else if ([key isEqualToString:@"PASSANGER_ACCIDENT"]){
            return @"Personal Accident to Passanger";
        }else if ([key isEqualToString:@"THIRD_INJURY"]){
            return @"Third Party Body Injury";
        }else if ([key isEqualToString:@"THIRD_DAMAGE"]){
            return @"Third Party Property Damage";
        }else if ([key isEqualToString:@"CAR_SCRATCH"]){
            return @"Car Scratch";
        }else if ([key isEqualToString:@"MORAL_DAMAGE"]){
            return @"Moral Damage";
        }else if ([key isEqualToString:@"OWN_DAMAGE"]){
            return @"Own Damage";
        }else if ([key isEqualToString:@"FIXTURE_ACCESSORIES"]){
            return @"Fixture and Accessories";
        }else if ([key isEqualToString:@"TRANSPORT_SERVICE"]){
            return @"Transport Service";
        }else if ([key isEqualToString:@"SELF_IGNITION_INSURANCE"]){
            return @"Self-Ignition";
        }else if ([key isEqualToString:@"ROBBERY_THEFT"]){
            return @"Robbery and Theft";
        }else if ([key isEqualToString:@"PEOPLE_LIABILITY"]){
            return @"Driver or Passenger Liability";
        }else if ([key isEqualToString:@"THIRD_LIABILITY"]){
            return @"Third Party Liability";
        }else if ([key isEqualToString:@"SCRATCH"]){
            return @"Scratch";
        }else if ([key isEqualToString:@"WINDOW_BREAKAGE"]){
            return @"Window Breakage";
        }else if ([key isEqualToString:@"MORAL_DAMAGE"]){
            return @"Moral Damage";
        }else if ([key isEqualToString:@"OWN_DAMAGE_CONTENT"]){
            return @"Loss or Damage to Insured Vehicle caused by collision and other perils.";
        }else if ([key isEqualToString:@"FIXTURE_ACCESSORIES_CONTENT"]){
            return @"Loss or Damage to Fixture and Accessories.";
        }else if ([key isEqualToString:@"TRANSPORT_SERVICE_CONTENT"]){
            return @"Transportation expenses, including the cost of a car rental, when your auto is  under repair.";
        }else if ([key isEqualToString:@"SELF_IGNITION_INSURANCE_CONTENT"]){
            return @"Loss or Damage to Insured Vehicle caused by self-ignition.";
        }else if ([key isEqualToString:@"ROBBERY_THEFT_CONTENT"]){
            return @"Loss or Damage to Insured Vehicle caused by robbery or theft.";
        }else if ([key isEqualToString:@"PEOPLE_LIABILITY_CONTENT"]){
            return @"Personal Accident to Driver or Passenger as a result of an accident involving your car.";
        }else if ([key isEqualToString:@"THIRD_LIABILITY_CONTENT"]){
            return @"Third Party Body Injury and Property Damage as a result of an accident involving your car.";
        }else if ([key isEqualToString:@"SCRATCH_CONTENT"]){
            return @"Scratch to Insured Vehicle in case of accidents excluding collision.";
        }else if ([key isEqualToString:@"WINDOW_BREAKAGE_CONTENT"]){
            return @"Window Breakage Repair";
        }else if ([key isEqualToString:@"MORAL_DAMAGE_CONTENT"]){
            return @"Third Party or Passenger Moral Damage as a result of an accident involving your car.";
        }else if ([key isEqualToString:@"PURCHASE"]){
            return @"%@ Buy";
        }else if ([key isEqualToString:@"SHOPPIONG_CART"]){
            return @"Shopping Cart";
        }else if ([key isEqualToString:@"COVERD"]){
            return @"What is covered";
        }else if ([key isEqualToString:@"ESTIMATED_PREMIUM"]){
            return @"Estimated Premium";
        }else if ([key isEqualToString:@"DRIVING_AREA"]){
            return @"Driving Area";
        }else if ([key isEqualToString:@"PLATE_NUMBER"]){
            return @"Vehicle Number Plate";
        }else if ([key isEqualToString:@"PURCHASE_DATE"]){
            return @"Vehicle Purchase Date";
        }else if ([key isEqualToString:@"MARKET_VALUE"]){
            return @"Market Value";
        }else if ([key isEqualToString:@"LIMIT"]){
            return @"Limit";
        }else if ([key isEqualToString:@"PREMIUM"]){
            return @"Premium";
        }else if ([key isEqualToString:@"ORDER_DETAIL"]){
            return @"Order Details";
        }else if ([key isEqualToString:@"CONFIRM_ORDER"]){
            return @"Confirm Order";
        }else if([key isEqualToString:@"ORDER_NUMBER"]){
            return @"Order Number";
        }else if ([key isEqualToString:@"SELECTED_COVERAGE"]){
            return @"Selected Coverage";
        }else if ([key isEqualToString:@"LIMIT_"]){
            return @"Limit";
        }else if ([key isEqualToString:@"COVERAGE_PREMIUM"]){
            return @"Coverage Premium";
        }else if ([key isEqualToString:@"TOTAL_PREMIUM"]){
            return @"Total Premium:";
        }else if ([key isEqualToString:@"PERIOD_OF_INSURANCE"]){
            return @"Period of Insurance";
        }else if ([key isEqualToString:@"CANCEL"]){
            return @"Cancel";
        }else if ([key isEqualToString:@"SELECT"]){
            return @"Select";
        }else if ([key isEqualToString:@"SYMBOL"]){
            return @"$";
        }else if ([key isEqualToString:@"NO_SELECTION"]){
            return @"Please select at least one";
        }else if ([key isEqualToString:@"FILL_BLANK"]){
            return @"Please fill in the complete";
        }
    }
    return @"";
}

- (NSString*)localizedLocationForKey:(NSString*)key
{
    if ([self.language isEqualToNumber:@(0)]) {
        return key;
    }else if ([self.language isEqualToNumber:@(1)]){
        if ([key isEqualToString:@"直辖市"]) {
            return @"Municipality";
        }else if ([key isEqualToString:@"北京"]){
            return @"BeiJing";
        }else if ([key isEqualToString:@"上海"]){
            return @"ShangHai";
        }else if ([key isEqualToString:@"天津"]){
            return @"TianJin";
        }else if ([key isEqualToString:@"重庆"]){
            return @"Chongqing";
        }else if ([key isEqualToString:@"特别行政区"]){
            return @"Special Administrative Region";
        }else if ([key isEqualToString:@"香港"]){
            return @"Hongkong";
        }else if ([key isEqualToString:@"澳门"]){
            return @"Macao";
        }else if ([key isEqualToString:@"台湾省"]){
            return @"TaiWan";
        }else if ([key isEqualToString:@"台北"]){
            return @"TaiPei";
        }else if ([key isEqualToString:@"安徽省"]){
            return @"Anhui";
        }else if ([key isEqualToString:@"合肥"]){
            return @"HeFei";
        }else if ([key isEqualToString:@"安庆"]){
            return @"AnQing";
        }else if ([key isEqualToString:@"蚌埠"]){
            return @"BengBu";
        }else if ([key isEqualToString:@"亳州"]){
            return @"HaoZhou";
        }else if ([key isEqualToString:@"巢湖"]){
            return @"ChaoHu";
        }else if ([key isEqualToString:@"滁州"]){
            return @"ChuZhou";
        }else if ([key isEqualToString:@"阜阳"]){
            return @"FuYang";
        }else if ([key isEqualToString:@"淮北"]){
            return @"HuaiBei";
        }else if ([key isEqualToString:@"淮南"]){
            return @"HuaiNan";
        }else if ([key isEqualToString:@"黄山"]){
            return @"HuangShan";
        }else if ([key isEqualToString:@"六安"]){
            return @"LiuAn";
        }else if ([key isEqualToString:@"马鞍山"]){
            return @"MaAnShan";
        }else if ([key isEqualToString:@"宿州"]){
            return @"SuZhou";
        }else if ([key isEqualToString:@"铜陵"]){
            return @"TongLing";
        }else if ([key isEqualToString:@"芜湖"]){
            return @"WuHu";
        }else if ([key isEqualToString:@"宣州"]){
            return @"XuanZhou";
        }else if ([key isEqualToString:@"福建省"]){
            return @"FuJian";
        }else if ([key isEqualToString:@"福州"]){
            return @"FuZhou";
        }else if ([key isEqualToString:@"龙海"]){
            return @"LongHai";
        }else if ([key isEqualToString:@"龙岩"]){
            return @"LongYan";
        }else if ([key isEqualToString:@"南平"]){
            return @"NanPing";
        }else if ([key isEqualToString:@"宁德"]){
            return @"NingDe";
        }else if ([key isEqualToString:@"莆田"]){
            return @"PuTian";
        }else if ([key isEqualToString:@"泉州"]){
            return @"QuanZhou";
        }else if ([key isEqualToString:@"三明"]){
            return @"SanMing";
        }else if ([key isEqualToString:@"漳州"]){
            return @"ZhangZhou";
        }else if ([key isEqualToString:@"厦门"]){
            return @"Xiamen";
        }else if ([key isEqualToString:@"甘肃省"]){
            return @"GanSu";
        }else if ([key isEqualToString:@"兰州"]){
            return @"LanZhou";
        }else if ([key isEqualToString:@"白银"]){
            return @"BaiYin";
        }else if ([key isEqualToString:@"嘉峪关"]){
            return @"JiaYuGuan";
        }else if ([key isEqualToString:@"金昌"]){
            return @"JinChang";
        }else if ([key isEqualToString:@"酒泉"]){
            return @"JiuQuan";
        }else if ([key isEqualToString:@"平凉"]){
            return @"PingLiang";
        }else if ([key isEqualToString:@"天水"]){
            return @"TianShui";
        }else if ([key isEqualToString:@"武威"]){
            return @"WuWei";
        }else if ([key isEqualToString:@"张掖"]){
            return @"ZhangYe";
        }else if ([key isEqualToString:@"广东省"]){
            return @"GuangDong";
        }else if ([key isEqualToString:@"广州"]){
            return @"GuangZhou";
        }else if ([key isEqualToString:@"潮州"]){
            return @"ChaoZhou";
        }else if ([key isEqualToString:@"东莞"]){
            return @"DongGuan";
        }else if ([key isEqualToString:@"佛山"]){
            return @"FoShan";
        }else if ([key isEqualToString:@"河源"]){
            return @"HeYuan";
        }else if ([key isEqualToString:@"惠州"]){
            return @"HuiZhou";
        }else if ([key isEqualToString:@"江门"]){
            return @"JiangMen";
        }else if ([key isEqualToString:@"茂名"]){
            return @"MaoMing";
        }else if ([key isEqualToString:@"梅州"]){
            return @"MeiZhou";
        }else if ([key isEqualToString:@"清远"]){
            return @"QingYuan";
        }else if ([key isEqualToString:@"汕头"]){
            return @"ShanTou";
        }else if ([key isEqualToString:@"汕尾"]){
            return @"ShanWei";
        }else if ([key isEqualToString:@"韶关"]){
            return @"ShaoGuan";
        }else if ([key isEqualToString:@"深圳"]){
            return @"ShenZhen";
        }else if ([key isEqualToString:@"顺德"]){
            return @"ShunDe";
        }else if ([key isEqualToString:@"阳江"]){
            return @"YangJiang";
        }else if ([key isEqualToString:@"云浮"]){
            return @"YunFu";
        }else if ([key isEqualToString:@"湛江"]){
            return @"ZhanJiang";
        }else if ([key isEqualToString:@"肇庆"]){
            return @"ZhaoQing";
        }else if ([key isEqualToString:@"中山"]){
            return @"ZhongShan";
        }else if ([key isEqualToString:@"珠海"]){
            return @"ZhuHai";
        }else if ([key isEqualToString:@"广西自治区"]){
            return @"GuangXi";
        }else if ([key isEqualToString:@"南宁"]){
            return @"NanNing";
        }else if ([key isEqualToString:@"北海"]){
            return @"BeiHai";
        }else if ([key isEqualToString:@"百色"]){
            return @"BaiSe";
        }else if ([key isEqualToString:@"防城港"]){
            return @"FangChengGang";
        }else if ([key isEqualToString:@"贵港"]){
            return @"GuiGang";
        }else if ([key isEqualToString:@"桂林"]){
            return @"GuiLin";
        }else if ([key isEqualToString:@"河池"]){
            return @"HeChi";
        }else if ([key isEqualToString:@"柳州"]){
            return @"LiuZhou";
        }else if ([key isEqualToString:@"钦州"]){
            return @"QinZhou";
        }else if ([key isEqualToString:@"梧州"]){
            return @"WuZhou";
        }else if ([key isEqualToString:@"玉林"]){
            return @"YuLin";
        }else if ([key isEqualToString:@"贵州省"]){
            return @"GuiZhou";
        }else if ([key isEqualToString:@"贵阳"]){
            return @"GuiYang";
        }else if ([key isEqualToString:@"安顺"]){
            return @"AnShun";
        }else if ([key isEqualToString:@"六盘水"]){
            return @"LiuPanShui";
        }else if ([key isEqualToString:@"兴义"]){
            return @"XingYi";
        }else if ([key isEqualToString:@"遵义"]){
            return @"ZunYi";
        }else if ([key isEqualToString:@"海南省"]){
            return @"HaiNan";
        }else if ([key isEqualToString:@"海口"]){
            return @"HaiKou";
        }else if ([key isEqualToString:@"儋州"]){
            return @"DanZhou";
        }else if ([key isEqualToString:@"琼海"]){
            return @"QiongHai";
        }else if ([key isEqualToString:@"琼山"]){
            return @"QiongShan";
        }else if ([key isEqualToString:@"三亚"]){
            return @"SanYa";
        }else if ([key isEqualToString:@"河北省"]){
            return @"HeBei";
        }else if ([key isEqualToString:@"石家庄"]){
            return @"ShiJiaZhuang";
        }else if ([key isEqualToString:@"保定"]){
            return @"BaoDing";
        }else if ([key isEqualToString:@"沧州"]){
            return @"CangZhou";
        }else if ([key isEqualToString:@"承德"]){
            return @"CengDe";
        }else if ([key isEqualToString:@"邯郸"]){
            return @"HanDan";
        }else if ([key isEqualToString:@"衡水"]){
            return @"HengShui";
        }else if ([key isEqualToString:@"廓坊"]){
            return @"KuoFang";
        }else if ([key isEqualToString:@"秦皇岛"]){
            return @"QinHuangDao";
        }else if ([key isEqualToString:@"唐山"]){
            return @"TangShan";
        }else if ([key isEqualToString:@"张家口"]){
            return @"ZhangJiaKou";
        }else if ([key isEqualToString:@"河南省"]){
            return @"HeNan";
        }else if ([key isEqualToString:@"郑州"]){
            return @"ZhengZhou";
        }else if ([key isEqualToString:@"安阳"]){
            return @"AnYang";
        }else if ([key isEqualToString:@"鹤壁"]){
            return @"HeBi";
        }else if ([key isEqualToString:@"焦作"]){
            return @"JiaoZuo";
        }else if ([key isEqualToString:@"济源"]){
            return @"JiYuan";
        }else if ([key isEqualToString:@"开封"]){
            return @"KaiFeng";
        }else if ([key isEqualToString:@"漯河"]){
            return @"LuoHe";
        }else if ([key isEqualToString:@"洛阳"]){
            return @"LuoYang";
        }else if ([key isEqualToString:@"南阳"]){
            return @"NanYang";
        }else if ([key isEqualToString:@"平顶山"]){
            return @"PingDingShan";
        }else if ([key isEqualToString:@"濮阳"]){
            return @"PuYang";
        }else if ([key isEqualToString:@"三门峡"]){
            return @"SanMenXia";
        }else if ([key isEqualToString:@"商丘"]){
            return @"ShangQiu";
        }else if ([key isEqualToString:@"新乡"]){
            return @"XinXiang";
        }else if ([key isEqualToString:@"信阳"]){
            return @"XinYang";
        }else if ([key isEqualToString:@"许昌"]){
            return @"XuChang";
        }else if ([key isEqualToString:@"周口"]){
            return @"ZhouKou";
        }else if ([key isEqualToString:@"驻马店"]){
            return @"ZhuMaDian";
        }else if ([key isEqualToString:@"黑龙江省"]){
            return @"HeiLongJiang";
        }else if ([key isEqualToString:@"哈尔滨"]){
            return @"Harbin";
        }else if ([key isEqualToString:@"大庆"]){
            return @"DaQing";
        }else if ([key isEqualToString:@"黑河"]){
            return @"HeiHe";
        }else if ([key isEqualToString:@"佳木斯"]){
            return @"JiaMuSi";
        }else if ([key isEqualToString:@"鸡西"]){
            return @"JiXi";
        }else if ([key isEqualToString:@"牡丹江"]){
            return @"MuDanJiang";
        }else if ([key isEqualToString:@"齐齐哈尔"]){
            return @"Qiqihar";
        }else if ([key isEqualToString:@"七台河"]){
            return @"QiTaiHe";
        }else if ([key isEqualToString:@"双鸭山"]){
            return @"ShuangYaShan";
        }else if ([key isEqualToString:@"绥芬河"]){
            return @"SuiFenHe";
        }else if ([key isEqualToString:@"绥化"]){
            return @"SuiHua";
        }else if ([key isEqualToString:@"湖北省"]){
            return @"HuBei";
        }else if ([key isEqualToString:@"武汉"]){
            return @"WuHan";
        }else if ([key isEqualToString:@"黄石"]){
            return @"HuangShi";
        }else if ([key isEqualToString:@"荆门"]){
            return @"JingMen";
        }else if ([key isEqualToString:@"十堰"]){
            return @"ShiYan";
        }else if ([key isEqualToString:@"随州"]){
            return @"SuiZhou";
        }else if ([key isEqualToString:@"天门"]){
            return @"TianMen";
        }else if ([key isEqualToString:@"襄樊"]){
            return @"XiangFan";
        }else if ([key isEqualToString:@"咸宁"]){
            return @"XianNing";
        }else if ([key isEqualToString:@"仙桃"]){
            return @"XianTao";
        }else if ([key isEqualToString:@"孝感"]){
            return @"XiaoGan";
        }else if ([key isEqualToString:@"宜昌"]){
            return @"YiChang";
        }else if ([key isEqualToString:@"宜城"]){
            return @"YiCheng";
        }else if ([key isEqualToString:@"枣阳"]){
            return @"ZaoYang";
        }else if ([key isEqualToString:@"湖南省"]){
            return @"HuNan";
        }else if ([key isEqualToString:@"长沙"]){
            return @"ChangSha";
        }else if ([key isEqualToString:@"常德"]){
            return @"ChangDe";
        }else if ([key isEqualToString:@"郴州"]){
            return @"ChenZhou";
        }else if ([key isEqualToString:@"衡阳"]){
            return @"HengYang";
        }else if ([key isEqualToString:@"怀化"]){
            return @"HuaiHua";
        }else if ([key isEqualToString:@"津市"]){
            return @"JinShi";
        }else if ([key isEqualToString:@"浏阳"]){
            return @"LiuYang";
        }else if ([key isEqualToString:@"娄底"]){
            return @"LouDi";
        }else if ([key isEqualToString:@"韶山"]){
            return @"ShaoShan";
        }else if ([key isEqualToString:@"邵阳"]){
            return @"ShaoYang";
        }else if ([key isEqualToString:@"湘潭"]){
            return @"XiangTan";
        }else if ([key isEqualToString:@"永州"]){
            return @"YongZhou";
        }else if ([key isEqualToString:@"岳阳"]){
            return @"YueYang";
        }else if ([key isEqualToString:@"张家界"]){
            return @"ZhangJiaJie";
        }else if ([key isEqualToString:@"株洲"]){
            return @"ZhuZhou";
        }else if ([key isEqualToString:@"吉林省"]){
            return @"JiLin";
        }else if ([key isEqualToString:@"长春"]){
            return @"ChangChun";
        }else if ([key isEqualToString:@"白城"]){
            return @"BaiCheng";
        }else if ([key isEqualToString:@"白山"]){
            return @"BaiShan";
        }else if ([key isEqualToString:@"敦化"]){
            return @"DunHua";
        }else if ([key isEqualToString:@"公主岭"]){
            return @"GongZhuLing";
        }else if ([key isEqualToString:@"吉林"]){
            return @"JiLin";
        }else if ([key isEqualToString:@"辽源"]){
            return @"LiaoYuan";
        }else if ([key isEqualToString:@"通化"]){
            return @"TongHua";
        }else if ([key isEqualToString:@"江苏省"]){
            return @"JiangSu";
        }else if ([key isEqualToString:@"南京"]){
            return @"NanJing";
        }else if ([key isEqualToString:@"常熟"]){
            return @"ChangShu";
        }else if ([key isEqualToString:@"常州"]){
            return @"ChangZhou";
        }else if ([key isEqualToString:@"淮安"]){
            return @"HuaiAn";
        }else if ([key isEqualToString:@"昆山"]){
            return @"KunShan";
        }else if ([key isEqualToString:@"连云港"]){
            return @"LianYunGang";
        }else if ([key isEqualToString:@"南通"]){
            return @"NanTong";
        }else if ([key isEqualToString:@"如皋"]){
            return @"RuGao";
        }else if ([key isEqualToString:@"宿迁"]){
            return @"SuQian";
        }else if ([key isEqualToString:@"苏州"]){
            return @"SuZhou";
        }else if ([key isEqualToString:@"太仓"]){
            return @"TaiCang";
        }else if ([key isEqualToString:@"泰兴"]){
            return @"TaiXing";
        }else if ([key isEqualToString:@"泰州"]){
            return @"TaiZhou";
        }else if ([key isEqualToString:@"通州"]){
            return @"TongZhou";
        }else if ([key isEqualToString:@"无锡"]){
            return @"WuXi";
        }else if ([key isEqualToString:@"徐州"]){
            return @"XuZhou";
        }else if ([key isEqualToString:@"扬州"]){
            return @"YangZhou";
        }else if ([key isEqualToString:@"张家港"]){
            return @"ZhangJiaGang";
        }else if ([key isEqualToString:@"镇江"]){
            return @"ZhenJiang";
        }else if ([key isEqualToString:@"江西省"]){
            return @"JiangXi";
        }else if ([key isEqualToString:@"南昌"]){
            return @"NanChang";
        }else if ([key isEqualToString:@"赣州"]){
            return @"GanZhou";
        }else if ([key isEqualToString:@"吉安"]){
            return @"JiAn";
        }else if ([key isEqualToString:@"景德镇"]){
            return @"JingDeZhen";
        }else if ([key isEqualToString:@"井冈山"]){
            return @"JingGangShan";
        }else if ([key isEqualToString:@"九江"]){
            return @"JiuJiang";
        }else if ([key isEqualToString:@"萍乡"]){
            return @"PingXiang";
        }else if ([key isEqualToString:@"瑞金"]){
            return @"RuiJin";
        }else if ([key isEqualToString:@"上饶"]){
            return @"ShangRao";
        }else if ([key isEqualToString:@"新余"]){
            return @"XinYu";
        }else if ([key isEqualToString:@"宜春"]){
            return @"YiChun";
        }else if ([key isEqualToString:@"鹰潭"]){
            return @"YingTan";
        }else if ([key isEqualToString:@"辽宁省"]){
            return @"LiaoNing";
        }else if ([key isEqualToString:@"沈阳"]){
            return @"ShenYang";
        }else if ([key isEqualToString:@"鞍山"]){
            return @"AnShan";
        }else if ([key isEqualToString:@"本溪"]){
            return @"BenXi";
        }else if ([key isEqualToString:@"朝阳"]){
            return @"ChaoYang";
        }else if ([key isEqualToString:@"大连"]){
            return @"DaLian";
        }else if ([key isEqualToString:@"丹东"]){
            return @"DanDong";
        }else if ([key isEqualToString:@"抚顺"]){
            return @"FuShun";
        }else if ([key isEqualToString:@"阜新"]){
            return @"FuXin";
        }else if ([key isEqualToString:@"葫芦岛"]){
            return @"HuLuDao";
        }else if ([key isEqualToString:@"锦州"]){
            return @"JinZhou";
        }else if ([key isEqualToString:@"辽阳"]){
            return @"LiaoYang";
        }else if ([key isEqualToString:@"盘锦"]){
            return @"PanJin";
        }else if ([key isEqualToString:@"铁岭"]){
            return @"TieLing";
        }else if ([key isEqualToString:@"营口"]){
            return @"YingKou";
        }else if ([key isEqualToString:@"内蒙古自治区"]){
            return @"InnerMongolia";
        }else if ([key isEqualToString:@"呼和浩特"]){
            return @"Hohehot Municipality";
        }else if ([key isEqualToString:@"包头"]){
            return @"BaoTou";
        }else if ([key isEqualToString:@"赤峰"]){
            return @"ChiFeng";
        }else if ([key isEqualToString:@"满洲里"]){
            return @"ManZhouLi";
        }else if ([key isEqualToString:@"通辽"]){
            return @"TongLiao";
        }else if ([key isEqualToString:@"乌兰浩特"]){
            return @"Ulan Hot";
        }else if ([key isEqualToString:@"乌海"]){
            return @"WuHai";
        }else if ([key isEqualToString:@"宁夏自治区"]){
            return @"NingXia";
        }else if ([key isEqualToString:@"银川"]){
            return @"YinChuan";
        }else if ([key isEqualToString:@"青铜峡"]){
            return @"Qingtong Gorge";
        }else if ([key isEqualToString:@"石嘴山"]){
            return @"ShiZuiShan";
        }else if ([key isEqualToString:@"吴忠"]){
            return @"WuZhou";
        }else if ([key isEqualToString:@"青海省"]){
            return @"QingHai";
        }else if ([key isEqualToString:@"西宁"]){
            return @"XiNing";
        }else if ([key isEqualToString:@"德令哈"]){
            return @"DeLingHa";
        }else if ([key isEqualToString:@"格尔木"]){
            return @"Golmud";
        }else if ([key isEqualToString:@"山东省"]){
            return @"ShanDong";
        }else if ([key isEqualToString:@"济南"]){
            return @"JiNan";
        }else if ([key isEqualToString:@"青岛"]){
            return @"QingDao";
        }else if ([key isEqualToString:@"安丘"]){
            return @"AnQiu";
        }else if ([key isEqualToString:@"滨州"]){
            return @"BinZhou";
        }else if ([key isEqualToString:@"昌邑"]){
            return @"ChangYi";
        }else if ([key isEqualToString:@"德州"]){
            return @"DeZhou";
        }else if ([key isEqualToString:@"东营"]){
            return @"DongYing";
        }else if ([key isEqualToString:@"菏泽"]){
            return @"HeZe";
        }else if ([key isEqualToString:@"济宁"]){
            return @"JiNing";
        }else if ([key isEqualToString:@"莱芜"]){
            return @"LaiWu";
        }else if ([key isEqualToString:@"聊城"]){
            return @"LiaoCheng";
        }else if ([key isEqualToString:@"临沂"]){
            return @"LinYi";
        }else if ([key isEqualToString:@"蓬莱"]){
            return @"PengLai";
        }else if ([key isEqualToString:@"曲阜"]){
            return @"QuFu";
        }else if ([key isEqualToString:@"日照"]){
            return @"RiZhao";
        }else if ([key isEqualToString:@"荣成"]){
            return @"RongCheng";
        }else if ([key isEqualToString:@"泰安"]){
            return @"TaiAn";
        }else if ([key isEqualToString:@"潍坊"]){
            return @"WeiFang";
        }else if ([key isEqualToString:@"威海"]){
            return @"WeiHai";
        }else if ([key isEqualToString:@"烟台"]){
            return @"YanTai";
        }else if ([key isEqualToString:@"枣庄"]){
            return @"ZaoZhuang";
        }else if ([key isEqualToString:@"淄博"]){
            return @"ZiBo";
        }else if ([key isEqualToString:@"山西省"]){
            return @"ShanXi";
        }else if ([key isEqualToString:@"太原"]){
            return @"TaiYuan";
        }else if ([key isEqualToString:@"大同"]){
            return @"DaTong";
        }else if ([key isEqualToString:@"晋城"]){
            return @"JinCheng";
        }else if ([key isEqualToString:@"临汾"]){
            return @"LinFen";
        }else if ([key isEqualToString:@"朔州"]){
            return @"ShuoZhou";
        }else if ([key isEqualToString:@"忻州"]){
            return @"XinZhou";
        }else if ([key isEqualToString:@"阳泉"]){
            return @"QuanYang";
        }else if ([key isEqualToString:@"运城"]){
            return @"YunCheng";
        }else if ([key isEqualToString:@"陕西省"]){
            return @"ShanXi";
        }else if ([key isEqualToString:@"西安"]){
            return @"XiAn";
        }else if ([key isEqualToString:@"咸阳"]){
            return @"XianYang";
        }else if ([key isEqualToString:@"安康"]){
            return @"AnKang";
        }else if ([key isEqualToString:@"宝鸡"]){
            return @"BaoJi";
        }else if ([key isEqualToString:@"汉中"]){
            return @"HanZhong";
        }else if ([key isEqualToString:@"商州"]){
            return @"ShangZhou";
        }else if ([key isEqualToString:@"铜川"]){
            return @"TongChuan";
        }else if ([key isEqualToString:@"渭南"]){
            return @"WeiNan";
        }else if ([key isEqualToString:@"延安"]){
            return @"YanAn";
        }else if ([key isEqualToString:@"榆林"]){
            return @"YuLin";
        }else if ([key isEqualToString:@"四川省"]){
            return @"SiChuan";
        }else if ([key isEqualToString:@"成都"]){
            return @"ChengDu";
        }else if ([key isEqualToString:@"巴中"]){
            return @"BaZhong";
        }else if ([key isEqualToString:@"崇州"]){
            return @"ChouZHou";
        }else if ([key isEqualToString:@"达川"]){
            return @"DaChuan";
        }else if ([key isEqualToString:@"德阳"]){
            return @"DeYang";
        }else if ([key isEqualToString:@"都江堰"]){
            return @"Dujiang Weir";
        }else if ([key isEqualToString:@"峨眉山"]){
            return @"Mountain Emei";
        }else if ([key isEqualToString:@"广元"]){
            return @"GuangYuan";
        }else if ([key isEqualToString:@"乐山"]){
            return @"Mountain Le";
        }else if ([key isEqualToString:@"泸州"]){
            return @"LuZhou";
        }else if ([key isEqualToString:@"绵阳"]){
            return @"MianYang";
        }else if ([key isEqualToString:@"南充"]){
            return @"NanChong";
        }else if ([key isEqualToString:@"内江"]){
            return @"NeiJiang";
        }else if ([key isEqualToString:@"攀枝花"]){
            return @"PanZhiHua";
        }else if ([key isEqualToString:@"雅安"]){
            return @"YaAn";
        }else if ([key isEqualToString:@"宜宾"]){
            return @"YiBin";
        }else if ([key isEqualToString:@"自贡"]){
            return @"ZiGong";
        }else if ([key isEqualToString:@"资阳"]){
            return @"ZiYang";
        }else if ([key isEqualToString:@"西藏自治区"]){
            return @"Tibet";
        }else if ([key isEqualToString:@"拉萨"]){
            return @"LaSa";
        }else if ([key isEqualToString:@"日喀则"]){
            return @"Shigatse";
        }else if ([key isEqualToString:@"新疆自治区"]){
            return @"Sinkiang";
        }else if ([key isEqualToString:@"乌鲁木齐"]){
            return @"Urumqi";
        }else if ([key isEqualToString:@"阿克苏"]){
            return @"Aksu";
        }else if ([key isEqualToString:@"阿勒泰"]){
            return @"Aletai";
        }else if ([key isEqualToString:@"阿图什"]){
            return @"Atushi";
        }else if ([key isEqualToString:@"博乐"]){
            return @"BoLe";
        }else if ([key isEqualToString:@"昌吉"]){
            return @"ChangJi";
        }else if ([key isEqualToString:@"阜康"]){
            return @"FuKang";
        }else if ([key isEqualToString:@"哈密"]){
            return @"HaMi";
        }else if ([key isEqualToString:@"和田"]){
            return @"HeTian";
        }else if ([key isEqualToString:@"克拉玛依"]){
            return @"Karamay";
        }else if ([key isEqualToString:@"喀什"]){
            return @"Kashgar";
        }else if ([key isEqualToString:@"库尔勒"]){
            return @"Korla";
        }else if ([key isEqualToString:@"奎屯"]){
            return @"KuiTun";
        }else if ([key isEqualToString:@"石河子"]){
            return @"ShiHeZi";
        }else if ([key isEqualToString:@"塔城"]){
            return @"TaCheng";
        }else if ([key isEqualToString:@"吐鲁番"]){
            return @"Turpan";
        }else if ([key isEqualToString:@"伊宁"]){
            return @"YiNing";
        }else if ([key isEqualToString:@"云南省"]){
            return @"YunNan";
        }else if ([key isEqualToString:@"昆明"]){
            return @"KunMing";
        }else if ([key isEqualToString:@"保山"]){
            return @"BaoShan";
        }else if ([key isEqualToString:@"大理"]){
            return @"DaLi";
        }else if ([key isEqualToString:@"曲靖"]){
            return @"QuJing";
        }else if ([key isEqualToString:@"思茅"]){
            return @"SiMao";
        }else if ([key isEqualToString:@"玉溪"]){
            return @"YuXi";
        }else if ([key isEqualToString:@"昭通"]){
            return @"ZhaoTong";
        }else if ([key isEqualToString:@"浙江省"]){
            return @"ZheJiang";
        }else if ([key isEqualToString:@"杭州"]){
            return @"HangZhou";
        }else if ([key isEqualToString:@"奉化"]){
            return @"FengHua";
        }else if ([key isEqualToString:@"湖州"]){
            return @"HuZhou";
        }else if ([key isEqualToString:@"嘉兴"]){
            return @"JiaXing";
        }else if ([key isEqualToString:@"金华"]){
            return @"JinHua";
        }else if ([key isEqualToString:@"丽水"]){
            return @"LiShui";
        }else if ([key isEqualToString:@"宁波"]){
            return @"NingBo";
        }else if ([key isEqualToString:@"衢州"]){
            return @"QuZhou";
        }else if ([key isEqualToString:@"瑞安"]){
            return @"RuiAn";
        }else if ([key isEqualToString:@"绍兴"]){
            return @"ShaoXing";
        }else if ([key isEqualToString:@"台州"]){
            return @"TaiZhou";
        }else if ([key isEqualToString:@"温州"]){
            return @"WenZhou";
        }else if ([key isEqualToString:@"义乌"]){
            return @"YiWu";
        }
    }
    return @"";
}

@end
