//
//  PlanFactor.h
//  EBTech
//
//  Created by shimingwei on 14-8-26.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanFactor : NSObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *subCode;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSArray *values;

- (void)updateWithDic:(NSDictionary*)dic;

@end
