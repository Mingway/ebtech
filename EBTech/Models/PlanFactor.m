//
//  PlanFactor.m
//  EBTech
//
//  Created by shimingwei on 14-8-26.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PlanFactor.h"

@interface PlanFactor ()

@end

@implementation PlanFactor

- (void)updateWithDic:(NSDictionary*)dic
{
    NSString *code = [dic objectForKey:@"code"];
    if (code != NULL) {
        self.code = code;
    }
    NSString *name = [dic objectForKey:@"name"];
    if (name != NULL) {
        self.name = name;
    }
    NSString *uuid = [dic objectForKey:@"uuid"];
    if (uuid != NULL) {
        self.uuid = uuid;
    }
    NSArray *limits = [dic objectForKey:@"limits"];
    if (limits != NULL) {
        if (limits.count >= 1) {
            NSArray *elements = [limits[0] objectForKey:@"elements"];
            if (elements && elements.count >= 1) {
                NSDictionary *element = elements[0];
                self.subCode = [element objectForKey:@"code"];
                NSDictionary *meta = [element objectForKey:@"meta"];
                NSString *type = [meta objectForKey:@"type"];
                if (type != NULL) {
                    self.type = type;
                    
                    if ([type isEqualToString:@"option"]) {
                        self.values = [meta objectForKey:@"values"];
                    }
                }
            }
        }
    }
}

@end
