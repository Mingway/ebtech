//
//  DataModel.h
//  EBTech
//
//  Created by shimingwei on 14-8-13.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModel : NSObject
@property (nonatomic, strong) NSNumber *language;

+ (instancetype)defaultModel;

- (void)setCarSelection:(NSNumber*)selection;
- (void)setCarAccidents:(NSArray *)selections;
- (void)setLoss:(NSArray *)selections;
- (void)setInsurance:(NSArray *)selections;


- (void)setState:(NSString*)state city:(NSString*)city dateStr:(NSString*)dateStr abbreviation:(NSString*)abbreviation plateNumber:(NSString*)plateNumber price:(NSString*)price;


- (NSNumber *)getCarSelection;
- (NSArray *)getCarAccidents;
- (NSArray *)getLoss;
- (NSArray *)getInsurance;

- (NSString *)getState;
- (NSString *)getCity;
- (NSString *)getDateStr;
- (NSString *)getAbbreviaiton;
- (NSString *)getPlateNumber;
- (NSString *)getPrice;

- (NSString*)localizedStringForKey:(NSString*)key;
- (NSString*)localizedLocationForKey:(NSString*)key;

@end
